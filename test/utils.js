import configureStore from 'redux-mock-store'; // eslint-disable-line
import createThunkMiddleware from 'src/redux/ravenThunkMiddleware';

export const mockStore = configureStore();
export const mockThunkStore = configureStore([createThunkMiddleware()]);
