import 'jest-styled-components'; // eslint-disable-line
import 'react-dates/initialize';
import 'matchmedia-polyfill';
import 'matchmedia-polyfill/matchMedia.addListener';
import { configure } from 'enzyme/build'; // eslint-disable-line
import Adapter from 'enzyme-adapter-react-16/build'; // eslint-disable-line

/**
   * Enzyme setup
   */

configure({ adapter: new Adapter() });
