import React from 'react';
import dayjs from 'dayjs';
import { startCase } from 'lodash';

import { Dropdown, DropdownRow } from 'src/components/PageComponents';
import { downloadFromLink } from 'src/services/api';
import { DATE_FORMAT } from './constants';

export function getProductMetric(product) {
  return `${product.unit_metric} ${product.metric_name}/${product.unit}`;
}

export function getCloudinaryThumbnail(url) {
  const TRANSFORM_STRING = 'c_fit,q_85,w_75';
  if (/https?:\/\/res\.cloudinary\.com\//.test(url)) {
    return url.replace('/upload', `/upload/${TRANSFORM_STRING}`);
  }

  return url;
}

export function capitalizeFirstLetter(str) {
  if (str && typeof str === 'string' && str.length > 0) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  return '';
}

export function getBasePath(pathname) {
  if (pathname) {
    return pathname.split('/')[1];
  }
  return '/';
}

export function getSearchBasePath(pathname) {
  const basePath = getBasePath(pathname);
  if (!['leads', 'accounts'].includes(basePath)) {
    return `/${basePath}`;
  }
  return '/products';
}

export function toFormattedDate(unformattedDate, dateFormat = DATE_FORMAT.INCLUDES_TIME_DATE) {
  return dayjs(unformattedDate).format(dateFormat);
}

export function toIndonesianCurrency(unformattedCurrency) {
  return new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
  }).format(unformattedCurrency);
}

export function prettifySnakeCase(str) {
  return startCase(str.replace(/_/g, ' '));
}

export function defaultSelectString(placeholder) {
  return `- Filter ${prettifySnakeCase(placeholder)} -`;
}

export function createOptionsWithUnassigned(options, placeholder) {
  return [defaultSelectString(placeholder), ...options];
}

export function createDropdownFilters(filters, currentParams, onChangeFilter) {
  return (
    <DropdownRow margin="0 0 1rem 0">
      {filters.map(({ value, options }) => (
        <Dropdown
          key={value}
          value={currentParams[value] || defaultSelectString(value)}
          onChange={onChangeFilter(value)}
        >
          {createOptionsWithUnassigned(options, value).map(item => (
            <option key={item} value={item}>
              {item}
            </option>
          ))}
        </Dropdown>
      ))}
    </DropdownRow>
  );
}

export async function downloadFromResponseBlob(link, filename) {
  try {
    const response = await downloadFromLink(link);
    const { data: blob } = response;
    const linkNode = document.createElement('a');
    if (linkNode.download !== undefined) {
      const url = window.URL.createObjectURL(blob);
      linkNode.setAttribute('href', url);
      linkNode.setAttribute('download', filename);
      linkNode.style.visibility = 'hidden';
      document.body.appendChild(linkNode);
      linkNode.click();
      document.body.removeChild(linkNode);
    }
  } catch (e) {
    throw e;
  }
}
