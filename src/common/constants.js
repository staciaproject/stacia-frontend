// Routes
export const ROUTES_WITH_SEARCH_BAR = ['products'];

// Products List
export const FILTER_DATE_RANGE = ['daily', 'yesterday', 'monthly'];
export const PAGE_SIZE = 20;

// Date Formats
export const DATE_FORMAT = {
  HH_mm_ss: 'HH:mm:ss',
  dddd_MMMM_DD: 'dddd, MMM DD',
  dddd_MMM_DD_HH_mm: 'dddd, MMM DD, HH:mm',
  dddd_DD_MMM_YYYY_HH_mm: 'dddd DD MMM YYYY, HH:mm',
  dddd_DD_MMM_YYYY: 'dddd DD MMM YYYY',
  DD_MMM_YYYY: 'DD MMM YYYY',
};

// Permissions
export const PERMISSIONS = {
  VIEW_STATISTIC: 'VIEW_STATISTIC',
  VIEW_ACTIVATOR: 'VIEW_ACTIVATOR',
  VIEW_LEAD: 'VIEW_LEAD',
  VIEW_ACCOUNT: 'VIEW_ACCOUNT',
};

// Menu type
export const MENU_TYPE = {
  MAIN: 'MAIN',
  DROPDOWN: 'DROPDOWN',
  ICON: 'ICON',
};
