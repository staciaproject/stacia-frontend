import merge from 'lodash/merge';

const base = {
  API_URL: 'https://api.staciamakeup.com/api/v1/',
};

const config = {
  production: {
    API_URL: 'https://api.staciamakeup.com/api/v1/',
  },
  local: {
    API_URL: 'http://localhost:8000/api/v1/',
  },
};

const mergedConfig = merge({}, base, config[process.env.NODE_ENV || 'development']);

export default mergedConfig;
