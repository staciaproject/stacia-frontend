import axios from 'axios';
import config from 'src/config';
import { logout } from 'src/redux/auth';

const instance = axios.create({
  baseURL: config.API_URL,
  responseType: 'json',
  timeout: 10000,
});

export default instance;

export function configureResponseInterceptor(store) {
  instance.interceptors.response.use(
    response => response,
    error => {
      if (error.response && error.response.status === 401) {
        store.dispatch(logout());
      }
      return Promise.reject(error);
    }
  );
}

// Auth
export async function login({ email, password }) {
  return instance.post('/users/login/', { email, password });
}

export async function register(payload) {
  return instance.post('/users/register/', payload);
}

export async function getPermissions(token) {
  return instance.get('/users/permissions/', { token });
}

// Products
export async function getProducts() {
  const response = await instance.get('/products/');
  return response;
}

export  async function getProductDetails(sku) {
  const response = await instance.get(`/products/${sku}`);
  return response;
}

// Articles
export async function getArticles() {
  const response = await instance.get('/article/');
  return response;
}

export  async function getArticleDetails(article) {
  const response = await instance.get(`/article/${article}`);
  return response;
}