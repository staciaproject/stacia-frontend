import { injectGlobal } from 'styled-components';
import { types } from './styles/theme';

/* eslint-disable no-unused-expressions */
injectGlobal`
  html {
    box-sizing: border-box;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    font-size: 16px;
    font-family: ${types.family.main};
  }

  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 700;
  }
`;
/* eslint-enable */
