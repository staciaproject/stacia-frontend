import 'babel-polyfill';
import 'react-dates/initialize';
import 'matchmedia-polyfill';
import 'matchmedia-polyfill/matchMedia.addListener';

import 'normalize.css';
import 'react-dates/lib/css/_datepicker.css';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import Raven from 'raven-js';

import configureStore from './redux/configureStore';
import { configureResponseInterceptor } from './services/api';
import history from './common/history';
import App from './containers/App';
import RavenBoundary from './components/RavenBoundary';

import './style';

Raven.config('https://5e8a71d9917e4eb2bd1dbd765f991db9@sentry.io/1375069', {
  environment: process.env.NODE_ENV,
  shouldSendCallback: () => process.env.NODE_ENV === 'production',
}).install();

const store = configureStore();
const rootNode = document.getElementById('app');
configureResponseInterceptor(store);

render(
  <RavenBoundary getState={store.getState}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </RavenBoundary>,
  rootNode
);
