import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { loadProducts } from 'src/redux/products';
import { Box, Row } from 'src/components/Box';
import ProductCard from 'src/components/ProductCard';
import MainProductCard from 'src/components/MainProductCard';
import AdminSidebar from 'src/components/AdminSidebar';
import { Table } from 'react-bootstrap';
import { Button, DropdownButton, MenuItem} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';


import AdminHeader from 'src/components/AdminHeader';
import AdminListCard from 'src/components/AdminListCard';


// TODO: Create loadAdmins function and API
// @connect(
//   state => ({
//     results: state.admins.results,
//   }),
//   { loadAdmins }
// )

class AdminList extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    // TODO: Create loadAdmins function and API
    // loadAdmins: PropTypes.func.isRequired,
  };
  
  componentDidMount() {
    // TODO: Create loadAdmins function and API
    // this.props.loadAdmins();
  };

  routeAddAdmin() {
    let path = '/eyelash';
    this.props.history.push(path);
  };

  render() {
    const boolean = true;

    return (
      <Wrapper>
        <AdminSidebar />
        <Content>
          <AdminHeader 
            title={'Add Admin'}
            icon={'fas fa-user-plus'}
            onClick={this.routeAddAdmin}/>
            <Table>
                <tr>
                    <td>
                        <h3>Admin List</h3>
                    </td>
                    <td style={{width:12+'rem'}}><Button variant="primary">
                        &#43; Add Admin</Button>
                    </td>
                </tr>
            </Table>
          <Row>
              <AdminListCard
                  name={'Stacia Sitohang'}
                  role={'Super Admin'}>
              </AdminListCard>
          </Row>
          <Row>
              <AdminListCard
                  name={'Admin 1'}
                  role={'Admin'}>
              </AdminListCard>
          </Row>

          <Row>
              <AdminListCard
                  name={'Admin 2'}
                  role={'Admin'}>
              </AdminListCard>
          </Row>
        </Content>
      </Wrapper>
    );
  }
};

const Wrapper = styled(Row)`
  height: 100%;
`;

const ResetButton = styled(Button)`
  background: transparent;
`

const Content = styled(Box)`
  border: 2px solid #e1d8e2;
  height: 100%;
  width: 70%:
  flex-shrink: 1;
  margin: 1rem;

  h3 {
    margin-left: 1rem;
  }

  Button {
    background: #603157;
    padding: 0.8rem;
    color: white;
    border-radius:0.5rem;
  }
`;

export default withRouter(AdminList);
