import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { loadProducts } from 'src/redux/products';
import { Box, Row } from 'src/components/Box';
import ProductCard from 'src/components/ProductCard';
import MainProductCard from 'src/components/MainProductCard';
import AdminSidebar from 'src/components/AdminSidebar';
import { Table } from 'react-bootstrap';
import { Button } from 'react-bootstrap';


import AdminCatalogCard from 'src/components/AdminCatalogCard';
import AdminCatalogHeader from 'src/components/AdminCatalogHeader';


@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class AdminInsertCatalog extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };
  componentDidMount() {
    this.props.loadProducts();
  }


  render() {
    const boolean = true;

    return (
      <Wrapper>
        <AdminSidebar />
        <Content>
          <AdminCatalogHeader 
            title={'Etalase'}
            icon={'fas fa-bookmark'}/>
            <Table>
              <tr>
                <td><h3>Daftar Etalase</h3></td>
                <td style={{width:12+'rem'}}><Button variant="primary"> &#43; Tambah Section</Button></td>
              </tr>
            </Table>
          <AdminCatalogCard
            title={'SR'}
            icon={'fas fa-user-plus'}
            to={'/addadmin'}>
          </AdminCatalogCard>
          <AdminCatalogCard
            title={'SR'}
            icon={'fas fa-user-plus'}
            to={'/addadmin'}>
          </AdminCatalogCard>
          <AdminCatalogCard
            title={'SR'}
            icon={'fas fa-user-plus'}
            to={'/addadmin'}>
          </AdminCatalogCard>
        </Content>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  border: 2px solid #e1d8e2;
  height: 100%;
  width: 70%:
  flex-shrink: 1;
  margin: 1rem;

  h3 {
    margin-left: 1rem;
  }

  Button {
    background: #603157;
    padding: 0.8rem;
    color: white;
    border-radius:0.5rem;
  }
`;


export default withRouter(AdminInsertCatalog);
