import React from 'react';
import styled from 'styled-components';
import { Box, Row } from 'src/components/Box';
import { withRouter } from 'react-router-dom';
import { privateRoutes as menu } from 'src/routes';

function Shop() {
  return (
    <Wrapper>
      <Content>
        <div className="online-shop">
          <p>Please visit our online shops:</p>
        </div>
        <div>
          <a href="https://shopee.co.id/staciaeyelash">
            <img src="/img/shopee.png" alt="shopee logo" className="online-shop-img"/> 
          </a>
          <a href="https://www.tokopedia.com/archive-staciaeyelash?">
            <img src="/img/tokopedia.png" alt="tokopedia logo" className="online-shop-img" />
          </a>  
        </div>
      </Content>
    </Wrapper>
  );
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  margin: 5%;
  padding: 5%;
  width: 100%;
  flex-shrink: 1;
  border: 4px solid #e7d2a1;
  color: #603157;
  align-items: center;
  line-height: 0.5;
  text-align: center;

  @media (max-width: 480px) {
    .online-shop {
      text-align: center
      margin: auto;
      p {
        font-size: 15px;
        line-height: 15px;
      }
    }
  
    .online-shop-img {
      height: 80px;
      margin: 20px;
    }
  }

  @media (min-width: 481px) {
    .online-shop {
      text-align: center
      margin: auto;
      p {
        font-size: 25px;
        line-height: 25px;
      }
    }
  
    .online-shop-img {
      height: 150px;
      margin: 40px;
    }
  }



`;

export default withRouter(Shop);
