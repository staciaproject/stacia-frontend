import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { loadProducts } from 'src/redux/products';
import { Box, Row } from 'src/components/Box';
import ProductCard from 'src/components/ProductCard';
import MainProductCard from 'src/components/MainProductCard';
import ProfileSidebar from 'src/components/ProfileSidebar';



@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class Wishlist extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };
  componentDidMount() {
    this.props.loadProducts();
  }


  render() {
    const boolean = true;

    return (
      <Wrapper pageName='AdminHome'>
        <ProfileSidebar />
        <Content>

        </Content>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  height: 100%;
  width: 70%:
  padding: 3rem;
  flex-shrink: 1;
`;

const Catalogue = styled(Row)`
  flex-wrap: wrap;
  width: 100%;
  padding: 2rem;
`;

export default withRouter(Wishlist);
