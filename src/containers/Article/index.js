import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Row } from 'react-grid-system';
import styled from 'styled-components';
import Loader from 'src/components/Loader';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Box } from 'src/components/Box';
import ArticleCard from 'src/components/ArticleCard';
import { loadProducts } from 'src/redux/products';

@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class Article extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.loadProducts();
  }

  render() {
    const {
      results: { data, isLoading, error, isLoaded },
    } = this.props;

    if (!isLoaded || isLoading) return <Loading />;
    if (error) return <Message text={`Error: ${error}`} />;

    return (
      <Wrapper>
        <Header> 
        </Header>
        <Content>
          <Articles width="100%">
            {data.slice(0).map((article, index) => (
              <ArticleCard
                image={article.images[0].url}
                title={article.name}
                summary={article.description}
                sku={`${index}`}
                date={article.dateCreated}
              />
            ))
            }
          </Articles>
        </Content>
      </Wrapper>
    );
  }
}

const Loading = () => (

  <Wrapper>
    <Header> 
    </Header>
    <Content>
        <Centered>
          <Loader size={10} />
        </Centered>
    </Content>
  </Wrapper>
);

const Wrapper = styled(Row)`
  height: 100%;
`;

const Header = styled(Box)`
  position: relative;
  text-align: center;
  color: white;

  .centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  width: 100%;
  height: 30rem;
  background-image: url('https://bitbucket.org/staciaproject/stacia-frontend/downloads/beauty.jpg');
  background-size: cover;

  @media (max-width: 768px) {
    height: 150px;
    h1 {
      font-size:16px;
    }
  }
`;

const Content = styled(Box)`
  height: 100%;
  width: 70%;
  padding: 3rem;
  flex-shrink: 1;
  align-items: center;
  margin: 0 auto;

  @media (max-width: 768px) {
    width: 100%;
    padding: 1.5rem;
    padding-right:2.5rem;
  }
`;

const Centered = styled(Row)`
  width: 10%;
  justify-content: center;
  align-items:center;
`;

const Articles = styled(Row)`
  flex-wrap: wrap;
  width: 100%;
  padding: 2rem;
`;

export default withRouter(Article);
