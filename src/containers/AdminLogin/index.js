import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Redirect } from 'react-router-dom';
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { login } from 'src/redux/auth';
import { Box } from 'src/components/Box';
import Button from 'src/components/Button';
import { media } from 'src/styles/theme';
import { TextInput, Label } from 'src/components/Input';
import { Row, Col } from 'react-grid-system';

@connect(
  ({ auth, router }) => ({
    auth,
    router,
  }),
  {
    login,
  }
)
export default class AdminLogin extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    router: PropTypes.shape({
      location: PropTypes.shape({
        state: PropTypes.object,
      }).isRequired,
    }).isRequired,
    login: PropTypes.func.isRequired,
  };

  onSubmit = async (values, { setSubmitting, setErrors }) => {
    try {
      await this.props.login(values);
    } catch (e) {
      setErrors(this.props.auth.error);
    }
    setSubmitting(false);
  };

  schema = Yup.object().shape({
    username: Yup.string().required(),
    password: Yup.string()
      .min(6)
      .required(),
  });

  render() {
    const {
      auth,
      router: { location },
    } = this.props;
    if (auth.isAuthenticated) {
      const to = (location.state && location.state.from) || '/';
      return <Redirect to={to} />;
    }

    const LoginForm = ({ values, handleChange }) => (
      <Box alignItems="center" width="100%" maxWidth="50rem">
        <h1>Login to Admin</h1>
        <p>Please enter your credentials to login.</p>
        <FluidForm>
          <TextInput
                value={values.email}
                onChange={handleChange}
                name="username"
                type="text"
                placeholder="User Name"
                className="input"
          />
          <ErrorMessage name="username" />
          <TextInput 
                value={values.password}
                onChange={handleChange}
                name="password"
                type="password"
                placeholder="Password"
                className="input"
          />
          <ErrorMessage name="password" />
          <Row>
              <Col className="left">  
                  <input type="checkbox"></input>Remember Me
              </Col>
              <Col className="right">
                  <Link to="/forgotpassword">Forgot Password</Link>
              </Col>
          </Row>
          {this.props.auth.error && <div>{this.props.auth.error.non_field_errors}</div>}
          <Box padding="1rem 0" width="10rem">
            <Button primary size="m" type="submit">
              Login
            </Button>
          </Box>
        </FluidForm>
      </Box>
    );

    return (
      <Wrapper>
        <LoginContainer>
            <Formik
            initialValues={{ username: '', password: '' }}
            validationSchema={this.schema}
            onSubmit={this.onSubmit}
            render={LoginForm}
            />
        </LoginContainer>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Box)`
  width: 100%;
`;

const LoginContainer = styled(Box)`
  margin: auto;
  margin-top: 5%;
  margin-bottom: 10%;
  width: 40%;
  border: 4px solid #e7d2a1;
  height: 100%;
  padding: 30px;
  text-align: center;
  color: #603157;
  font-family: Raleway;

  h1 {
      margin: 0;
  }

  p {
      color: #777777;
  }

  .input {
      margin-bottom: 10px;
  }

  .logo {
    height: 80px;
    width: auto;
    margin-bottom: 8%;
  }
`;

const FluidForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
