import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import styled from 'styled-components';
import { Box, Row, Col } from 'src/components/Box';
import { ContentCard } from 'src/components/ContentCard';
import ArticleCard from 'src/components/ArticleCard';
import { NavLink } from 'react-router-dom';
import { media } from 'src/styles/theme';

export default function MakeupArtistry() {
  const handleOnDragStart = e => e.preventDefault();
  // const iframeStyle = {
  //   border: 'none',
  //   overflow: 'hidden',
  //   width: '100%',
  //   height: '50rem'
  // };

  return (
    <Content>
      <img
        src="https://bitbucket.org/staciaproject/stacia-frontend/downloads/makeup.png"
        onDragStart={handleOnDragStart}
        className="yours-custom-class"
        alt="makeup"
      />
      <ContentCard>
        <h1 className="title" style={{fontWeight:'lighter', color:'grey'}}>About Me</h1>
        <Row maxWidth="100%" className="main-row">
          <BoxImage>
            <img src="https://bitbucket.org/staciaproject/stacia-frontend/downloads/stacia.png" className="profile-about" alt="stacia-avatar"/>
          </BoxImage>
          <Box padding="0rem 2rem" className="main-box">
            <h1 padding="1rem 0" style={{color:'#693d61', marginBottom:'0'}}>Stacia</h1>
            <h2 style={{color:'grey'}}>Edina Hasiana Sitohang</h2>
              <Box padding="1rem 0rem" className='box-summary'>
                <p style={{color:'grey'}} align="justify">
                Stacia Sitohang is a professional Make Up Artist (MUA) and Kebaya Designer based in Jakarta. 
                Specialized in Batak Wedding Makeup, she herself cares about Batak culture and actively preserve the culture. 
                Stacia Makeup Artistry is the best place for any modern Batak bride needs and wants. 
                </p>
                <p style={{color:'grey'}} align="justify">
                Stacia has learnt makeup since 2009 and fully dedicated herself as a fulltime MUA since 2017. 
                Certified by numerous professional MUA such as Nanath Nadia, Berti Makeup, etc.
                </p>
              </Box>
              <Box className="row-box">
                <Row>
                  <img src="/img/icon-bridal.png" alt="icon-bridal" className="icon-small"/>
                  <Box justifyContent="center">
                    <h2 style={{color:'#693d61'}}>Bridal Makeup</h2>
                  </Box>
                </Row>
                <Row>
                  <img src="/img/icon-graduation.png" alt="icon-graduation" className="icon-small" />
                  <Box justifyContent="center">
                    <h2 style={{color:'#693d61'}}>Graduation Makeup</h2>
                  </Box>
                </Row>
                <Row>
                  <img src="/img/icon-specialmakeup.png" className="icon-small" style={{paddingLeft:'5px'}} />
                  <Box justifyContent="center">
                    <h2 style={{color:'#693d61'}}>Special Makeup</h2>
                  </Box>
                </Row>
              </Box>
          </Box>
        </Row>
      </ContentCard>
      <ContentCard>
        <Row padding="1.5rem 0">
          <h1 className="title" style={{fontWeight:'lighter', color:'grey'}}>Portfolio</h1>
          <InstaHandle>
            <img src="/img/icon-instagram.svg" alt="icon-instagram" style={{height:'30px'}}/>
            <a href="https://instagram.com/staciamakeup/">
              <span>@staciamakeup</span>
            </a>
          </InstaHandle>
        </Row>
        <iframe
          title="insta-widget"
          src="https://snapwidget.com/embed/792817"
          className="snapwidget-widget"
          scrolling="yes"
        />
      </ContentCard>
      <ContentCard style={{color:'#693d61'}}>
        <h1 className="title" style={{fontWeight:'lighter', color:'grey'}}>Contact Me</h1>
        <Row style={{paddingTop:'1rem', paddingBottom:'1rem', borderTop:'2px solid #e7d2a1', borderBottom: '2px solid #e7d2a1'}}> 
          <Col style={{width:'60%'}}>
            <span style={{marginBottom:'auto', marginTop:'auto', marginRight:'1rem', color:'grey'}}>
            Come to me to have flawless appearance that is sophisticated, modern, elegant and stylish.
            I can&#39;t wait to hear from you, whatever the occasion!
            </span>
          </Col>
          <Col className="footer-contact">
            <Row className="contact" style={{marginTop:'1rem'}}>
              <i class="fas fa-phone"></i>
              <Box>
                <a style={{fontWeight:'bolder'}}> Call Me</a>
                <span>+62 811 220 9966</span>
              </Box>
            </Row>
            <Row className="contact" style={{marginTop:'1rem'}}>
              <i class="fas fa-map-marker-alt"></i>
              <Box>
                <a style={{fontWeight:'bolder'}}>Address</a>
                <span>Jl. Bukitduri Selatan no 24
                <br />(Adenium Corner), Jakarta Selatan</span>
              </Box>
            </Row>
            <Row className="contact" style={{marginTop:'1rem'}}>
              <i class="fas fa-envelope"></i>
              <Box>
                <a style={{fontWeight:'bolder'}}>Email Address</a>
                <span>brushedbystacia@gmail.com</span>
              </Box>
            </Row>
          </Col>
          {/* <Col style={{marginLeft:'10rem', width:'70%'}}>
            <Row className="contact">
              <Col style={{width:'50%'}}>
                <input placeholder='First Name' type='text'></input>
              </Col>
              <Col style={{width:'50%'}}>
                <input placeholder='Last Name' type='text'></input>
              </Col>
            </Row>
            <Row className="contact">
              <Col style={{width:'50%'}}>
                <input placeholder='Your Email' type='text'></input>
              </Col>
              <Col style={{width:'50%'}}>
                <input placeholder='Subject' type='text'></input>
              </Col>
            </Row>
            <textarea placeholder='Your Message' type='text'></textarea>
            <Row>
              <Button to='#'>Submit > </Button>
            </Row>
          </Col> */}
        </Row>
      </ContentCard>
    </Content>
  );
}

const Button = styled(NavLink)`
  border:1px solid #693d61;
  color: white;
  background: #693d61;
  margin-top: 1rem;
  padding: 1rem;
  text-decoration: None;
`


const BoxImage =styled(Box)`
  height:inherit;
  width:50%;
  justify-content: center;

  @media (max-width:768px) {
    width:100%;
  }
`

const Content = styled(Box)`
  display: flex;
  width: 100%;
  font-size: 18px;

  .snapwidget-widget {
    border: none;
    overflow: hidden;
    width: 100%;
    height: 50rem;
  }


  input, textarea {
    border: 1px solid #e7d2a1;
    border-radius: 5px;
    margin-right: 1rem;
    height:2rem;
    margin-top: 1rem;
    padding:0.5rem;
  }

  textarea {
    height: 5rem;
  }

  .background {
    background-size: 100% auto;
    background-repeat: no-repeat;
    opacity: 0.2;
  }

  span {
    font-family: 'Raleway', sans-serif;
  }

  .title {
    font-size: 3rem;
    font-weight: 100;
  }

  .contact {
    align-items: center;
    img {
      height: 4rem;
      padding-right: 1rem;
    }
  }

  .profile-about {
    height: auto;
    width: 100%;
  }

  .yours-custom-class {
    height:auto;
    width: 100%;
  }

  .main-box {
    max-width: 50%;
    h2 {
      margin-top: 0.5rem;
      font-size: 18px;
    }
    p {
      line-height: 1.5rem;
    }
  }

  i {
    min-width:2rem;
  }

  .footer-contact {
    margin-left: 3rem;
    float: right;
  }

  @media (max-width: 768px) {
    font-size: 0.5rem;

    .footer-contact {
      margin-left: 0rem;
      font-size:0.45rem;
    }

    .yours-custom-class {
      height:auto;
      width: 100%
    }
    

    .contact {
      font-size: 0.45rem;
    }


    .title {
      font-size: 1.25rem;
      font-weight: 100;
    }

    .profile-about {
      height: auto;
      width: 100%;
    }

    .content-makeup {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .snapwidget-widget {
      border: none;
      overflow: hidden;
      width: 100%;
      height: 15rem;
    }

    .main-row {
      flex-direction: column;
    }
    i {
      min-width:1rem;
    }

    .main-box {
      padding: 0rem 1rem;
      max-width: 100%;
      h1 {
        font-size:1.25rem;
        margin-bottom: 0;
        margin-top: 2rem;
      }
      
      h2 {
        font-size:1.15rem;
        margin-top:0.2rem;
      }

      p {
        font-size: 0.75rem;
        line-height: 1.25rem;
        text-align: justify;
        margin: 0;
        margin-right: 1rem;
      }

      .icon-small {
        height: 100px;
      }
      .box-price {
        padding: 0;
        text-align: left;
        font-size: 1.25rem;
      }
    }
  }


  @media (max-width: 1204px, min-width:769){
    .footer-contact {
      float: right;
      margin-left:3rem;
    }

    .yours-custom-class {
      height:auto;
      width: 100%
    }
    
    .contact {
      font-size: 12px;
    }

    .title {
      font-size: 18px;
      font-weight: 100;
    }

    .profile-about {
      height: auto;
      width: 100%;
    }

    .content-makeup {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .snapwidget-widget {
      border: none;
      overflow: hidden;
      width: 100%;
      height: 15rem;
    }


    i {
      min-width:1rem;
    }

    .main-box {
      padding:0 1rem;
      width:100%;
      padding-left:2rem;

      h1 {
        font-size:16px;
        margin-bottom: 0;
        margin-top: 2rem;
      }
      
      h2 {
        font-size:18px;
        margin-top:0.2rem;
      }

      p {
        width: 100%;
        font-size: 12px;
        line-height: 16px;
        text-align: justify;
        margin: 0;
      }

      .icon-small {
        height: 100px;
      }
      .box-price {
        padding: 0;
        text-align: left;
        font-size: 12px;
      }
    }
  }
`;

const Carousel = styled(Box)`
  img {
    width: 100%;
  }
`;

const InstaHandle = styled(Row)`
  align-items: center;
  justify-content: center;
  width: 100%;
  img {
    height: 2rem;
    padding-right: 1rem;
  }
  a {
    text-decoration: none;
    color: ${props => props.theme.colors.purple(0)};
  }
`;
