import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { loadProducts } from 'src/redux/products';
import { Box, Row } from 'src/components/Box';
import ProductCard from 'src/components/ProductCard';
import MainProductCard from 'src/components/MainProductCard';
import AdminSidebar from 'src/components/AdminSidebar';
import { Table } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';


import AdminHeader from 'src/components/AdminHeader';
import EmailEditor from 'react-email-editor'


@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class AdminBlog extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };
  componentDidMount() {
    this.props.loadProducts();
  }


  render() {
    const boolean = true;

    return (
      <Wrapper>
        <AdminSidebar />
        <Content>
          <AdminHeader 
            title={'Email Template'}
            icon={'fas fa-bookmark'}/>
            <Table>
              <EmailEditor
                ref={editor => this.editor = editor}
              />
            </Table>
        </Content>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const ResetButton = styled(Button)`
  background: transparent;
`

const Content = styled(Box)`
  border: 2px solid #e1d8e2;
  width: 70%:
  flex-shrink: 1;
  margin: 1rem;

  h3 {
    margin-left: 1rem;
  }

  Button {
    background: #603157;
    padding: 0.8rem;
    color: white;
    border-radius:0.5rem;
  }
`;


export default withRouter(AdminBlog);
