import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Box, Row } from 'src/components/Box';
import Loader from 'src/components/Loader';
import MainProductCard from 'src/components/MainProductCard';
import ProductCard from 'src/components/ProductCard';
import Sidebar from 'src/components/Sidebar';
import { loadProducts } from 'src/redux/products';
import styled from 'styled-components';

@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class Products extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.loadProducts();
  }

  render() {
    const {
      results: { data, isLoading, error, isLoaded },
    } = this.props;

    if (!isLoaded || isLoading) return <Loading />;
    if (error) return <Message text={`Error: ${error}`} />;
    if (!data || data.length === 0) return <Message text="There is no product. Come back later." />;

    const mainProduct = data[0];

    return (
      <Wrapper>
        <Sidebar />
        <Content>
          <Row>
            <MainProductCard
              sku={mainProduct.sku}
              reviews={mainProduct.reviews}
              name={mainProduct.name}
              price={mainProduct.price}
              description={mainProduct.description}
              img={mainProduct.images[0] ? mainProduct.images[0].url : 'Cari default image'}
              isLiked={mainProduct.isLiked}
              likes={mainProduct.likes}
              rating={mainProduct.rating}
            />
          </Row>
          <Catalogue width="70%">
            {data.slice(1).map(product => (
              <ProductCard
                sku={product.sku}
                reviews={product.reviews}
                key={product.sku}
                name={product.name}
                price={product.price}
                description={product.description}
                img={product.images[0] ? product.images[0].url : 'Cari default image'}
                sale={false}
                isLiked={product.isLiked}
                likes={product.likes}
                rating={product.rating}
              />
            ))}
          </Catalogue>
        </Content>
      </Wrapper>
    );
  }
}

const Loading = () => (
  <Wrapper>
    <Sidebar />
    <Centered>
      <Loader size={20} />
    </Centered>
  </Wrapper>
);

const Message = ({ text }) => (
  <Wrapper>
    <Sidebar />
    <Centered>
      <h4>{text}</h4>
    </Centered>
  </Wrapper>
);

Message.propTypes = { text: PropTypes.string.isRequired };

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  height: 100%;
  width: 100%:
  padding: 3rem;
  flex-shrink: 1;
`;

const Centered = styled(Row)`
  width: 100%;
  justify-content: center;
`;

const Catalogue = styled(Row)`
  flex-wrap: wrap;
  width: 100%;
  padding: 2rem;
`;

export default withRouter(Products);
