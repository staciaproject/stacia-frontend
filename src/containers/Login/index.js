import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { login } from 'src/redux/auth';
import { Box } from 'src/components/Box';
import Button from 'src/components/Button';
import Footer from 'src/components/Footer';
import { media } from 'src/styles/theme';
import { TextInput, Label } from 'src/components/Input';
import { Row, Col } from 'react-grid-system';
import { exportDefaultSpecifier } from '@babel/types';

@connect(
  ({ auth, router }) => ({
    auth,
    router,
  }),
  {
    login,
  }
)
export default class Login extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    router: PropTypes.shape({
      location: PropTypes.shape({
        state: PropTypes.object,
      }).isRequired,
    }).isRequired,
    login: PropTypes.func.isRequired,
  };

  onSubmit = async (values, { setSubmitting, setErrors }) => {
    try {
      await this.props.login(values);
    } catch (e) {
      setErrors(this.props.auth.error);
    }
    setSubmitting(false);
  };

  schema = Yup.object().shape({
    email: Yup.string().required(),
    password: Yup.string()
      .min(6)
      .required(),
  });

  render() {
    const {
      auth,
      router: { location },
    } = this.props;
    if (auth.isAuthenticated) {
      const to = (location.state && location.state.from) || '/';
      return <Redirect to={to} />;
    }

    const LoginForm = ({ values, handleChange }) => (
      <Box alignItems="flex-start" width="100%" maxWidth="20rem">
        <h2>please, login to your account</h2>
        <FluidForm>
          <Label>
            email
            <TextInput
              value={values.email}
              onChange={handleChange}
              name="email"
              type="text"
              placeholder="Masukkan email"
            />
            <ErrorMessage name="email" />
          </Label>
          <Label>
            password
            <TextInput
              value={values.password}
              onChange={handleChange}
              name="password"
              type="password"
              placeholder="Password"
            />
            <ErrorMessage name="password" />
          </Label>
          {this.props.auth.error && <div>{this.props.auth.error.non_field_errors}</div>}
          <Box padding="1rem 0" width="10rem">
            <Button primary size="m" type="submit">
              Login
            </Button>
          </Box>
        </FluidForm>
        <p>
          If you don&#39;t have an account, please{' '}
          <Link to="/register">
            <b>sign up</b>
          </Link>
        </p>
      </Box>
    );

    return (
      <Wrapper>
        <LoginContainer>
          <Row>
            <Col sm={6}>
              <img className="left" src="/img/login.png" alt="login"/>
            </Col>
            <Col sm={6}>
              <img className="logo" src="/img/logo.png" alt="logo"/>
              <Formik
                initialValues={{ email: '', password: '' }}
                validationSchema={this.schema}
                onSubmit={this.onSubmit}
                render={LoginForm}
              />
            </Col>
          </Row>
        </LoginContainer>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Box)`
  justify-content: center;
`;

const LoginContainer = styled(Box)`
  justify-content: center;
  color: #603157;
  width: 100%;

  .left {
    width: 100%;
    height: auto;
    ${media('phonePortrait')} {
      display: none;
    }
  }

  .logo {
    height: 80px;
    width: auto;
    margin-bottom: 8%;
  }
`;

const FluidForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
