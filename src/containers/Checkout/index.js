import React, { Component } from 'react';
import styled from 'styled-components';
import IconDelete from 'react-icons/lib/go/trashcan';
import { GlowingBox } from 'src/components/GlowingBox';
import { Box, Row } from 'src/components/Box';
import Sidebar from 'src/components/Sidebar';
import Button from 'src/components/Button';
import CreateAddressModal from 'src/components/Modals/CreateAddressModal';

export default class Checkout extends Component {
  render() {
    return (
      <Content>
        <Sidebar />
        <Box>
          <StyledGlowingBox>
            <Row>
              <h2 style={{paddingBottom:'1rem'}}>Stacia Sitohang</h2>
              <CreateAddressModal />
            </Row>
            <table style={{width:'1000px', marginBottom:'2rem'}}>
              <thead>
                <tr>
                  <th></th>
                  <th>Receiver</th>
                  <th>Address</th> 
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>
                    <Box>
                      <span>Stacia Sitohang</span>
                      <span>+62 888 777 666</span>
                    </Box>
                  </td>
                  <td>
                    <Box>
                      <span>Kantor</span>
                      <span>Jl. Sumur Bandung No. 3</span>
                      <span>Bandung, West Java 40134</span>
                    </Box>
                  </td>
                  <td>
                    <Row style={{justifyContent:'center'}}>
                      <div><i class='fas fa-edit' style={{paddingRight:'0.5rem'}}></i></div>
                      <div><i class='fas fa-trash-alt' ></i></div>
                    </Row>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <Box>
                      <span>Stacia Sitohang</span>
                      <span>+62 888 777 666</span>
                    </Box>
                  </td>
                  <td>
                    <Box>
                      <span>Kantor</span>
                      <span>Jl. Sumur Bandung No. 3</span>
                      <span>Bandung, West Java 40134</span>
                    </Box>
                  </td>
                  <td>
                    <Row style={{justifyContent:'center'}}>
                      <div><i class='fas fa-edit' style={{paddingRight:'0.5rem'}}></i></div>
                      <div><i class='fas fa-trash-alt'></i></div>
                    </Row>
                  </td>
                </tr>
              </tbody>
            </table>
          </StyledGlowingBox>
        </Box>
      </Content>
    );
  }
}

const Content = styled(Row)`
  width: 100%;
  .background {
    background-size: 100% auto;
    background-repeat: no-repeat;
    opacity: 0.2;
  }
  table {
    font-family: raleway;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid ${props => props.theme.colors.grey(5)};
    text-align: left;
    padding: 8px;
  }

  th {
    font-weight: lighter;
    color: grey;
  }

  h2 {
    font-weight: 500;
  }

  tr, h2 {
    color: #603157;
  }
`;

const StyledGlowingBox = styled(Box)`
  border 1px solid #e7d2a1;
  margin: 2rem 4rem;
  padding: 1rem 2rem;
  box-shadow: 0 4px 8px 0 rgba(171, 183, 183, 1);
  width: 100%;
  justify-content: center;
`;
