import React, { Component } from 'react';
import { Row, Col } from 'react-grid-system';
import styled from 'styled-components';
import { Box } from 'src/components/Box';
import Loader from 'src/components/Loader';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { loadProducts } from 'src/redux/products';

@connect(
    state => ({
      results: state.products.results,
    }),
    { loadProducts }
)

class ArticleContent extends Component {
    static propTypes = {
      results: PropTypes.object.isRequired,
      loadProducts: PropTypes.func.isRequired,
    };
    
    componentDidMount() {
      this.props.loadProducts();
    }
    
    render() {
      const {
          results: { data, isLoading, error, isLoaded },
      } = this.props;

      if (!isLoaded || isLoading) return <Loading />;
      if (error) return <Message text={`Error: ${error}`} />;

      const id = this.props.match.params.id;
      const articleDetails = data[this.props.match.params.id];

      var count = 0;
  
      return (
          <Wrapper>
            <Header>
                <Title>
                    <h1>{articleDetails.name}</h1>
                    <p>{articleDetails.description.split('.').slice(0,2).join(".")}</p>
                    {articleDetails.images.slice(0,1).map(image => (
                      <img src={image.url}>
                      </img>
                    ))}
                </Title>
                <MainContent>
                  <Top>
                      <div className="ib writer">
                          <img src="https://bitbucket.org/staciaproject/stacia-frontend/downloads/writer.png" className="writer-img"></img>
                          <h2>Stacia Sitohang</h2>
                          <p style={{justifyContent:'justify'}}>Stacia Sitohang is a professional Make Up Artist (MUA) and Kebaya Designer based in Jakarta.</p>
                      </div>
                      <div className="ib summary">
                          <p className="date">{formatDate(articleDetails.dateCreated)}
                            <Line className="linedate"/>
                          </p>
                          <p style={{justifyContent:'justify'}}>
                            <pre style={{whiteSpace:'pre-line'}}>
                              <div dangerouslySetInnerHTML={{ __html: articleDetails.description.split('.').slice(2).join(".").trim()}} className="article-content"/>  
                            </pre>
                          </p>
                          {articleDetails.images.slice(1,2).map(image => (
                            <img src={image.url}>
                            </img>
                          ))}
                      </div>
                      <div className="ib summary">
                          <p style={{justifyContent:'justify'}}>
                            <pre style={{whiteSpace:'pre-line'}}>
                              <div dangerouslySetInnerHTML={{ __html: articleDetails.howToUse}} className="article-content"/>  
                            </pre>
                          </p>
                      </div>
                      <div className="ib summary">
                        {articleDetails.images.slice(2,3).map(image => (
                          <iframe width="100%" className="iframe-youtube" height="315" src={image.url} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        ))}
                      </div>
                  </Top>
                </MainContent>
                <FurtherReading>
                  <h2 style={{float:'center', color:'#603157', textAlign:'center'}}>FURTHER READING</h2>

                  <Row style={{textAlign:'center', justifyContent:'center'}}>
                  {data.slice(0).map(function(article, index) {
                    if ((index != id) && (count < 3)) {
                      count = count + 1;
                      return (<FurtherReadingCard>
                        <Col>
                          <BoxImage>
                            <img src={article.images[0].url}></img>
                          </BoxImage>
                          <h2>{article.name}</h2>
                          <p style={{textAlign:'left', fontSize:'12px', marginBottom:'1rem'}}>{formatDate(articleDetails.dateCreated)}
                          </p>
                          <p className="furtherreading">
                          {article.description.slice(0,150)} . . .
                          </p>
                          <Link className="link" to={`/tips/${index}`}>Read More &#62;&#62;</Link>
                        </Col>
                      </FurtherReadingCard>)
                    }
                  }.bind(this))
                  }
                  </Row>
                </FurtherReading>
            </Header>   
        </Wrapper>
      );
    }
}


function formatDate(string){
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  return new Date(string).toLocaleDateString([],options);
}

const Loading = () => (
  <Wrapper>
    <Header> 
    </Header>
    <Content>
        <Centered>
          <Loader size={10} />
        </Centered>
    </Content>
  </Wrapper>
);


const FurtherReading = styled(Box)`
  width: 75%;
  margin: auto;
  border-top: 0px;
  margin-top: 5rem;
  justify-content: center;
  text-align: center;

  @media (max-width: 480px) {
    margin-top: 3rem;
  }
`


const FurtherReadingCard = styled(Box)`
  height: 40rem;
  width: 30%;
  img {
    width: 100%;
    height: auto;
    background-position: center center;
    background-repeat: no-repeat;
  }

  h2 {
    font-size: 16px;
    text-align:left;
  }

  .link {
    text-decoration:none; 
    color:grey; 
    float:left;
    background: #603157;
    color: white;
    padding: 0.5rem;
    border-radius: 10px;
    font-size: 14px;
  }

  .furtherreading {
    text-align:justify;
    font-size:14px;
    margin-bottom:1.5rem
  }

  @media (max-width: 768px) {
    width: 100%;
    height: 40rem;

    h2 {
      text-align:left;
    }

    .furtherreading {
      text-align:justify;
      font-size:12px;
      margin-bottom:1rem
    }

    .link {
      text-decoration:none; 
      color:grey; 
      float:left;
      background: #603157;
      color: white;
      padding: 0.5rem;
      border-radius: 8px;
      font-size: 10px;
    }
  }

  @media (max-width: 480px) {
    width: 100%;
    height: 30rem;


    h2 {
      text-align:left;
    }

    .furtherreading {
      text-align:justify;
      font-size:10px;
      margin-bottom:1rem
    }

    .link {
      text-decoration:none; 
      color:grey; 
      float:left;
      background: #603157;
      color: white;
      padding: 0.5rem;
      border-radius: 8px;
      font-size: 8px;
    }
  }
`

const BoxImage = styled(Box)`
  width:100%;
  height:250px;
  justify-content:center;
  background-color:#e7d2a1;

  @media (max-width: 768px) {
    height:27rem;
  }

  @media (max-width: 480px) {
    height:250px;
  }

`

const MainContent = styled(Box)`
  width: 75%;
  margin: auto;
  border: 4px solid #e7d2a1;
  border-top: 0px;
`

const Content = styled(Box)`
  height: 100%;
  width: 70%;
  padding: 3rem;
  flex-shrink: 1;
  align-items: center;
  margin: 0 auto;

  @media (max-width: 480px) {
    width: 100%;
    padding: 1rem;
  }
`;

const Wrapper = styled(Row)`
  height: 100%;
`;


const Title = styled(Box)`
    width: 75%;
    margin: auto;
    border-left: 4px solid #e7d2a1;
    font-size: 24px;
    padding-bottom: 0px;
    border-left: 4px solid #e7d2a1;
    heigth: 100%;
    h1 {
      font-size:40px;
      margin-bottom:0;
      padding-left: 5%;
    }
    p {
      font-size: 16px;
      padding-left: 5%;
      padding-right: 25%;
    }
    img {
        margin-top: 4rem;
        width: 120%;
        height: auto;
        overflow: hidden;
    }

    @media (max-width: 480px) {
        font-size: 12px;
        padding-left: 0;

        h1 {
          font-size:14px;
          margin-bottom:0;
          padding-left: 5%;
        }

        p {
          font-size: 10px;
          padding-left: 5%;
          padding-right: 20%;
        }

        img {
          margin-top: 1rem;
          width: 120%;
          height: auto;
          overflow: hidden;
        }
    }
`;

const Top = styled.div`
  height: 100%;
  bottom: 0;
  width: 100%;
  padding: 10px;
  align-items: center;
  overflow: hidden;
  padding-top: 0px;

  .article-content {
    text-align: justify;
  }

  .writer {
      width: 25%;
      margin-left: 2%;
      margin-right: 2%;
      border:4px solid #e7d2a1;
      border-top: 0px;
      border-right: 0px;
      text-align: center;
      color:#603157;
      font-size:16px;
      font-weight: bolder;
      float: left;

      p {
        text-align: justify;
        padding-left: 20px;
        padding-right: 20px;
        color:black;
        font-size:14px;
        font-weight: bolder;
        float: left;
      }

      h2 {
        padding-left: 20px;
        padding-right: 20px;
        text-align: left;
        color:#603157;
        font-size:18px;
        font-weight: bolder;
        float: left;
        margin-bottom: 0px;
      }
  }

  .writer-img {
      height:auto;
      padding-left: 20px;
      width:100%
  }

  .summary {
      width: 65%;
      margin-right: 2%;
      padding: 25px;
      text-align: justify;
      color:#A9A9A9A;
      font-size:16px;
      font-weight: bolder;
      margin-left: 30%;

      br {
        margin:1.5em 0;
        line-height:2.5em;
      }

      img {
        width: 100%;
        padding-top:35px;
        height: auto;
      }


      pre {
        font-family: Raleway;
        line-height: 1.5;
        white-space: pre-wrap;
        text-align: justify;
      }

      h1 {
        font-size: 20px;
        margin:0;
      }

      .date {
        text-align:left;
        font-size:14px;
        margin-bottom:4rem
      }

      .linedate {
        float: right;
      }
  }

  @media (max-width: 768px) {
    .summary {
      width:68%;
      padding-right:10px;
      .linedate {
        float: right;
        width:60%;
      }
    }

    .article-content {
      font-size:14px;
      text-align: justify;
    }

    .writer {
      width: 30%;
      p {
        font-size: 12px;
      }
    }
  }

  @media (max-width: 480px) {
    .writer {
      width: 60%;
      margin-left: 2%;
      margin-right: 2%;
      border:4px solid #e7d2a1;
      border-top: 0px;
      text-align: center;
      color:#603157;
      font-size:16px;
      font-weight: bolder;
      float: left;

      h2 {
        padding-left: 20px;
        padding-right: 20px;
        text-align: left;
        color:#603157;
        font-size:10px;
        font-weight: bolder;
        float: left;
        margin-bottom: 0px;
      }

      p {
        text-align: justify;
        padding-left: 20px;
        padding-right: 20px;
        margin-top:5px;
        color:black;
        font-size:10px;
        font-weight: bolder;
        float: left;
      }
    }

    .writer-img {
      height:auto;
      float: left;
      padding-left: 20px;
      width:87%;
    }

    .summary {
      width: 100%;
      padding: 0px;
      padding-right: 10px;
      text-align: justify;
      color:#A9A9A9A;
      font-size:10px;
      font-weight: bolder;
      margin-left: 5px;

      .iframe-youtube {
        width: 100%;
        height: 150px;
      }

      .article-content {
        padding-top:3rem;
        text-align: justify;
      }

      pre {
        font-family: Raleway;
        line-height: 1.5;
        white-space: pre-wrap;
      }

      h1 {
        font-size: 12px;
        margin:0;
      }

      .date {
        text-align:left;
        font-size:10px;
        margin-bottom:8rem;
      }

      .linedate {
        float: none;
        width:25%;
      }
    }


    
  }
`;

const Middle = styled(Box)`
  margin: 5%;
  
  img {
      height: 500px;
  }
`;

const Bottom = styled(Box)`
  margin-left: 10%;
  margin-right: 10%;
  margin-top: 1rem;

  br {
    margin:1.5em 0;
    line-height:2.5em;
  }  
  
  .iframe-youtube {
    margin-top:1rem;
    margin-bottom:1rem;
    width: 100%;
    height: 700px;
  }

  @media (max-width: 480px) {
    font-size: 12px;

    .iframe-youtube {
        margin-top:1rem;
        margin-bottom:1rem;
        width: 100%;
        height: 200px;
      }
  }

`;

const Author = styled(Box)`
  margin-left: 10%;
  margin-right: 10%;

  p {
      margin-bottom:0;
      margin-top:0;
      font-size: 12px;
      color: #603157;
      text-align:center;
  }

  @media (max-width: 480px) {
    font-size: 12px;

    p {
        margin-bottom:0;
        margin-top:0;
        font-size: 8px;
        color: #603157;
        text-align:center;
    }
}

`;

const Quote = styled(Box)`
    font-size: 32px;
    font-weight: bold;
    color:#603157;
    text-align: center;
    border-top: 2px solid #A9A9A9;
    border-bottom: 2px solid #A9A9A9;

    @media (max-width: 480px) {
        font-size: 14px;
    }
`

const GlowingLine = styled(Box)`
  height: 8px;
  width: 8%;
  margin: 0.5rem 0;
  background-color: #eeb124;
  background-image: linear-gradient(
    to right,
    #e7d2a1,
    #faf8cb,
    #e7d2a1,
    #faf8cb,
    #e7d2a1,
    #faf8cb,
    #e7d2a1,
    #faf8cb
  );

  @media (max-width: 480px) {
    height: 8px;
    width: 25%;
  }
  
`;

const Line = styled(Box)`
  height: 2px;
  width: 80%;
  margin: 0.5rem 0;
  background-color: #A9A9A9;
`

const Tips = styled(Box)`
   margin-right: 10%;
   margin-left: 20%;
   margin-bottom: 3%;
   border-left: 4px solid #e7d2a1;
   border-right: 4px solid #e7d2a1;
   border-bottom: 4px solid #e7d2a1;
   height: 100%;
   padding-left: 20px;
   padding-bottom: 3%;
   p {
        padding-right: 20%;
   }
   img {
       width: 70%;
       height: auto;
   }
   .writer {
       width: 100%;
       height: auto;
   }
   .text-right {
       text-align: right;
   }

   .red {
       color: red;
   }
`;

const Image = styled(Box)`
    margin-left: 20%;
    border-left: 4px solid #e7d2a1;
    heigth: 300px;
    width: auto;
`;

const Header = styled(Box)`
    position: relative;
    text-align: justify;
    justify-content: center;
    color: grey;

    width: 100%;
    height: 100%;
    background-image: url('/img/articles/article8.png');
    background-size: cover;

    font-family: Raleway;
    line-height: 1.5;

    h1, h2, h3 {
        color: #603157;
    }
`;

const Centered = styled(Row)`
  width: 10%;
  justify-content: center;
  align-items:center;
`;

export default withRouter(ArticleContent);