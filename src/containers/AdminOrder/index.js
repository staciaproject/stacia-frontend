import React, { Component } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { loadProducts } from 'src/redux/products';
import { Box, Row } from 'src/components/Box';
import ProductCard from 'src/components/ProductCard';
import MainProductCard from 'src/components/MainProductCard';
import AdminSidebar from 'src/components/AdminSidebar';
import { Table } from 'react-bootstrap';
import { Button, DropdownButton, MenuItem} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';


import AdminHeader from 'src/components/AdminHeader';


@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class AdminOrder extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };
  componentDidMount() {
    this.props.loadProducts();
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    // TODO: Change to apply filter based on selected option
    console.log(`Option selected:`, selectedOption);
  };

  render() {
    const boolean = true;

    const filterSelect = [
      { label: "All", value: 1 },
      { label: "Partial", value: 2 }
    ];

    return (
      <Wrapper>
        <AdminSidebar />
        <Content>
          <AdminHeader 
            title={'Orders'}
            icon={'fas fa-bookmark'}/>
            <Table>
                <tr>
                  <td style={{padding:'1rem'}}>
                    Status
                    <Button style={{marginLeft:'0.5rem'}}>Semua Pesanan</Button>
                    <Button style={{marginLeft:'0.5rem'}}>Pesanan Baru</Button>
                    <Button style={{marginLeft:'0.5rem'}}>Siap Dikirim</Button>
                    <Button style={{marginLeft:'0.5rem'}}>Dalam Pengiriman</Button>
                    <Button style={{marginLeft:'0.5rem'}}>Pesanan Selesai</Button>
                    <Button style={{marginLeft:'0.5rem'}}>Pesanan Dibatalkan</Button>
                    <ResetButton style={{marginLeft:'0.5rem', background:'transparent', color:'#603157', border:'0px solid'}}>Reset</ResetButton>
                  </td>
                </tr>
                <tr>
                  <td style={{padding:'1rem1'}}>
                    <Select 
                      placeholder='Pilih Filter'
                      style={{marginLeft:'0.5rem'}} 
                      options={filterSelect}
                      onChange={this.handleChange} />
                  </td>
                </tr>
            </Table>
        </Content>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const ResetButton = styled(Button)`
  background: transparent;
`

const Content = styled(Box)`
  border: 2px solid #e1d8e2;
  height: 100%;
  width: 70%:
  flex-shrink: 1;
  margin: 1rem;

  h3 {
    margin-left: 1rem;
  }

  Button {
    background: #603157;
    padding: 0.8rem;
    color: white;
    border-radius:0.5rem;
  }
`;


export default withRouter(AdminOrder);
