import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import { publicRoutes, privateRoutes } from 'src/routes';
import theme from 'src/styles/theme';
import AuthenticatedRoute from 'src/components/AuthenticatedRoute';
import { reloadAuth, logout } from 'src/redux/auth';
import DashboardLayout from 'src/components/DashboardLayout';

@withRouter
@connect(
  state => ({
    isAuthenticated: state.auth.isAuthenticated,
  }),
  {
    reloadAuth,
    logout,
  }
)
class App extends Component {
  static propTypes = {
    isAdmin: PropTypes.bool.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
    reloadAuth: PropTypes.func.isRequired,
  };

  state = {
    isCheckingAuth: true,
    isAdmin: false,
  };

  componentDidMount() {
    const token = window.localStorage.getItem('token');
    const user = window.localStorage.getItem('user');
    if (token && user) {
      this.props.reloadAuth({ token, user });
    }
    this.setState({ isCheckingAuth: false });

    // TODO: Change isAdmin to show different navbar
    this.setState({ isAdmin: false })
  }

  render() {
    const { isAuthenticated } = this.props.isAuthenticated;
    if (this.state.isCheckingAuth) return <h3>Loading...</h3>;

    return (
      <ThemeProvider theme={theme}>
        <Switch>
          {privateRoutes.map(({ component: Comp, ...route }) => (
            <AuthenticatedRoute
              key={route.path}
              authenticated={isAuthenticated}
              {...route}
              component={props => (
                <DashboardLayout
                  isAdmin={this.state.isAdmin}>
                  <Comp {...props} />
                </DashboardLayout>
              )}
            />
          ))}
          {publicRoutes.map(route => (
            <Route key={route.path} {...route} />
          ))}
        </Switch>
      </ThemeProvider>
    );
  }
}

export default hot(module)(App);
