import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import IconDelete from 'react-icons/lib/go/trashcan';
import { GlowingBox } from 'src/components/GlowingBox';
import { Box, Row } from 'src/components/Box';
import Sidebar from 'src/components/Sidebar';
import Button from 'src/components/Button';

export default class Cart extends Component {

  constructor(props) {
    super(props);
    this.state = {inputVal:'10'};
  }

  render() {
    return (
      <Content>
        <Sidebar />
        <Box>
          <StyledGlowingBox>
            <h2 style={{paddingBottom:'1rem'}}>Shopping Cart</h2>
            <table style={{width:'1000px', marginBottom:'2rem'}}>
              <thead>
                <tr>
                  <th></th>
                  <th>Image</th>
                  <th>Product</th> 
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><i class='fas fa-trash-alt'></i></td>
                  <td>Eyelash Image</td>
                  <td>Stacia Eyelashes SR-01</td> 
                  <td>IDR 50.000</td>
                  <td><input style={{width:'50px'}} type="number" value={this.state.inputVal} onChange={(e) => {this.setState({inputVal: e.target.value})}} /></td>
                  <td>IDR 50.000</td>
                </tr>
                <tr>
                  <td><i class='fas fa-trash-alt'></i></td>
                  <td>Eyelash Image</td>
                  <td>Stacia Eyelashes SR-01</td> 
                  <td>IDR 50.000</td>
                  <td><input style={{width:'50px'}} type="number" value={this.state.inputVal} onChange={(e) => {this.setState({inputVal: e.target.value})}} /></td>
                  <td>IDR 50.000</td>
                </tr>
              </tbody>
            </table>
            <h2 style={{paddingBottom:'1rem'}}>Cart Totals</h2>
            <table>
              <tbody>
                <tr>
                  <th>Subtotal</th>
                  <td>IDR 150.000</td>
                </tr>
                <tr>
                  <th>Shipping</th>
                  <td>IDR 10.000</td>
                </tr>
                <tr>
                  <th>Total:</th>
                  <td>IDR 160.000</td>
                </tr>
              </tbody>
            </table>
            <CheckoutButton><Link to="/checkout" className="checkout">PROCEED TO CHECKOUT</Link></CheckoutButton>
          </StyledGlowingBox>
        </Box>
      </Content>
    );
  }
}

const Content = styled(Row)`
  width: 100%;
  font-family: raleway;
  
  .background {
    background-size: 100% auto;
    background-repeat: no-repeat;
    opacity: 0.2;
  }
  table {
    border-collapse: collapse;
    width: 100%;
  }
  th {
    font-weight: lighter;
    color: grey;
  }

  h2 {
    font-weight: 500;
  }

  tr, h2 {
    color: #603157;
  }

  td, th {
    border: 1px solid ${props => props.theme.colors.grey(5)};
    text-align: left;
    padding: 8px;
  }
`;

const StyledGlowingBox = styled(Box)`
  border 1px solid #e7d2a1;
  margin: 2rem 4rem;
  padding: 1rem 2rem;
  box-shadow: 0 4px 8px 0 rgba(171, 183, 183, 1);
  width: 100%;
  justify-content: center;
`;

const CheckoutButton = styled(Button)`
  margin: 1rem auto;
  padding: auto 3rem;
  outline: none;
  border-radius: 25px;
  font-size: 10px;
  font-weight: 100;
  .checkout {
    color: white;
  }
  a {
    text-decoration: none;
  }
`;
