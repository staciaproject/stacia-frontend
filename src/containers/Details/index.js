import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FILTER_DATE_RANGE } from 'src/common/constants';
import { Box, Row } from 'src/components/Box';
import Sidebar from 'src/components/Sidebar';
import MainDetails from 'src/components/MainDetails';
import RatingAndReviews from 'src/components/RatingAndReviews';
import { loadProductDetails } from 'src/redux/products';
import Loader from 'src/components/Loader';

@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProductDetails }
)

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: FILTER_DATE_RANGE[0],
    };
  }

  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProductDetails: PropTypes.func.isRequired,
  };

  componentDidMount() {
    // The parameter id from url '/eyelashdetail/:id
    alert(this.props.match.params.id)
    this.props.loadProductDetails(this.props.match.params.id);
  }

  render() {
    const {
      results: { data, isLoading, error, isLoaded },
    } = this.props;

    if (!isLoaded || isLoading) return <Loading />;
    if (error) return <Message text={`Error: ${error}`} />;
    if (!data || data.length === 0) return <Message text="There is no product. Come back later." />;

    // DEBUG
    const productDetails = data[0];
    // TODO: Get product details by id or find out which index the specific product is located

    return (
        <Wrapper>
            <Sidebar />
            <Content>
                <MainDetails 
                  sku={productDetails.sku}
                  name={productDetails.name}
                  description={productDetails.description}
                  price={productDetails.price}
                  stock={productDetails.stock}
                  images={productDetails.images}
                  reviews={productDetails.reviews}
                  isReviewed={productDetails.isReviewed}
                  likes={productDetails.likes}
                  isLiked={productDetails.isLiked}
                  rating={productDetails.rating}
                  howToUse={productDetails.howToUse}
                />
                <RatingAndReviews />
            </Content>
        </Wrapper>
    );
  }
}

const Loading = () => (
  <Wrapper>
    <Sidebar />
    <Centered>
      <Loader size={20} />
    </Centered>
  </Wrapper>
);

const Message = ({ text }) => (
  <Wrapper>
    <Sidebar />
    <Centered>
      <h4>{text}</h4>
    </Centered>
  </Wrapper>
);

Message.propTypes = { text: PropTypes.string.isRequired };

const Wrapper = styled(Row)`
  height: 100%;
  width: 100%;
`;

const Content = styled(Box)`
  height: 100%;
  width: 100%;
  padding: 3rem;
  flex-shrink: 1;
`;

const Centered = styled(Row)`
  width: 100%;
  justify-content: center;
`;

export default withRouter(Details);
