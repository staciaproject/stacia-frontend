import React from 'react';
import styled from 'styled-components';
import { Box, Row } from 'src/components/Box';
import { withRouter, Link } from 'react-router-dom';
import { privateRoutes as menu } from 'src/routes';
import { Table } from 'react-bootstrap';
import Button from 'src/components/Button';

function PaymentDetail() {
  return (
    <Wrapper>
      <Content>
        <h1>Payment Detail</h1>
        <Table className="table" responsive>
          <thead>
            <tr>
              <th />
              <th>Image</th>
              <th>Product</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr className="box">
              <td>Icon</td>
              <td>Image</td>
              <td>Stacia Eyelashes SR-01</td>
              <td>IDR 50.000</td>
              <td>1</td>
              <td>IDR 50.000</td>
            </tr>
            <tr className="box">
              <td>Icon</td>
              <td>Image</td>
              <td>Stacia Eyelashes SR-01</td>
              <td>IDR 50.000</td>
              <td>1</td>
              <td>IDR 50.000</td>
            </tr>
            <tr className="box">
              <td>Icon</td>
              <td>Image</td>
              <td>Stacia Eyelashes SR-01</td>
              <td>IDR 50.000</td>
              <td>1</td>
              <td>IDR 50.000</td>
            </tr>
          </tbody>
        </Table>
        <CheckoutButton>
          <Link to="/checkout" className="checkout">
            PROCEED TO CHECKOUT
          </Link>
        </CheckoutButton>
      </Content>
    </Wrapper>
  );
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  margin: 5%;
  padding: 5%;
  padding-top: 1%;
  width: 100%;
  flex-shrink: 1;
  margin-bottom: 15%;
  border: 4px solid #e7d2a1;
  color: #603157;
  line-height: 2;

  h1 {
    font-weight: lighter;
  }

  th {
    color: grey;
    font-weight: lighter;
  }

  .box {
    text-align: center;
    box-shadow: 0.5px 0.5px 0.5px 0.5px rgba(210, 215, 211, 1);
    margin-bottom: 10px;
  }

  table {
    width: 100%;
    border-collapse: separate;
    border-spacing: 10px;
  }
`;

const CheckoutButton = styled(Button)`
  margin: 1rem auto;
  padding: auto 3rem;
  outline: none;
  border-radius: 25px;
  font-size: 10px;
  font-weight: 100;
  .checkout {
    color: white;
  }
  a {
    text-decoration: none;
  }
`;

export default withRouter(PaymentDetail);
