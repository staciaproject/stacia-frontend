import React, { Component } from 'react';
import styled from 'styled-components';

import { FILTER_DATE_RANGE } from 'src/common/constants';
import { Box } from 'src/components/Box';
import EyelashCarousel from 'src/components/EyelashCarousel';
import Testimonial from 'src/components/Testimonial';



class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: FILTER_DATE_RANGE[0],
    };
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(_, prevState) {
    if (prevState.filter !== this.state.filter) {
      this.loadData();
    }
  }

  onFilterChange = evt => {
    this.setState({ filter: evt.target.value });
  };

  loadData = () => {
    const { filter: date_range } = this.state;
  };

  render() {
    return (
      <Content>
        <a href="/shop">
          <SpecialOffer>
            <img alt="offer" src="https://bitbucket.org/staciaproject/stacia-frontend/downloads/offer.jpg" />
          </SpecialOffer>
        </a>
        <a href="/shop">
          <MonthlyOffer>
            <img alt="monthly-promo" src="https://bitbucket.org/staciaproject/stacia-frontend/downloads/promomonth.jpg" />
          </MonthlyOffer>
        </a>
        <EyelashCarousel />
        <Testimonial />
      </Content>
    );
  }
}

const Content = styled(Box)`
  display: flex;
  width: 100%;

  .background {
    background-size: 100% auto;
    background-repeat: no-repeat;
    opacity: 0.2;
  }

`;

const MonthlyOffer = styled(Box)`
  align-items: center;
  width: 100%;
  background-size: 100% auto;
  background-repeat: no-repeat;
  img {
    width: 70%;
    margin-top: 10%;
    padding-bottom: 10%;
  }
`;

const SpecialOffer = styled(Box)`
  align-items: center;
  background-image: url('https://bitbucket.org/staciaproject/stacia-frontend/downloads/home.jpg');
  width: 100%;
  background-size: 100% auto;
  background-repeat: no-repeat;
  img {
    width: 70%;
    margin-top: 45%;
  }
`;


export default Home;
