import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { loadProducts } from 'src/redux/products';
import { Box, Row } from 'src/components/Box';
import ProductCard from 'src/components/ProductCard';
import MainProductCard from 'src/components/MainProductCard';
import AdminSidebar from 'src/components/AdminSidebar';


import AdminHomeMenu from 'src/components/AdminHomeMenu';
import AddUser from '../../../public/img/add-user.png';


@connect(
  state => ({
    results: state.products.results,
  }),
  { loadProducts }
)
class AdminHome extends Component {
  static propTypes = {
    results: PropTypes.object.isRequired,
    loadProducts: PropTypes.func.isRequired,
  };
  componentDidMount() {
    this.props.loadProducts();
  }


  render() {
    const boolean = true;

    return (
      <Wrapper pageName='AdminHome'>
        <AdminSidebar />
        <Content>
          <Row>
            <AdminHomeMenu
              title={'Add Admin'}
              icon={'fas fa-user-plus'}
              to={'/addadmin'}>
            </AdminHomeMenu> 
            <AdminHomeMenu
              title={'Insert Catalog'}
              icon={'fas fa-cart-plus'}
              to={'/product'}>
            </AdminHomeMenu> 
            <AdminHomeMenu
              title={'Insert Portfolio'}
              icon={'fas fa-camera'}
              to={'/portfolio'}>
            </AdminHomeMenu>
          </Row>
          <Row> 
          <AdminHomeMenu
              title={'Email Template'}
              icon={'fas fa-envelope'}
              to={'/email'}>
            </AdminHomeMenu>
            <AdminHomeMenu
              title={'Orders'}
              icon={'fas fa-box-open'}
              to={'/orders'}>
            </AdminHomeMenu> 
            <AdminHomeMenu
              title={'Payment'}
              icon={'fas fa-money-bill'}
              to={'/paymeny'}>
            </AdminHomeMenu> 
          </Row>
          <Row>
            <AdminHomeMenu
              title={'Blog'}
              icon={'fas fa-newspaper'}
              to={'/blog'}>
            </AdminHomeMenu> 
          </Row>
        </Content>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  height: 100%;
  width: 70%:
  padding: 3rem;
  flex-shrink: 1;
`;

const Catalogue = styled(Row)`
  flex-wrap: wrap;
  width: 100%;
  padding: 2rem;
`;

export default withRouter(AdminHome);
