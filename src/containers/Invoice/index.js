import React, { Component } from 'react';
import styled from 'styled-components';
import { Box, Row } from 'src/components/Box';
import { withRouter } from 'react-router-dom';
import { Table } from 'react-bootstrap';

function Invoice() {
  return (
    <Wrapper>
      <Content>
        <h1>Order History</h1>
        <Table className="table" responsive>
          <thead>
            <tr>
              <th>Invoice</th>
              <th>Invoice Number</th>
              <th>Date</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <tr className="box">
              <td>Stacia OEI 1</td>
              <td>Stacia-123456</td>
              <td>16/09/2018</td>
              <td>PENDING</td>
            </tr>
            <tr className="box">
              <td>Stacia OEI 1</td>
              <td>Stacia-123456</td>
              <td>16/09/2018</td>
              <td>DONE</td>
            </tr>
            <tr className="box">
              <td>Stacia OEI 1</td>
              <td>Stacia-123456</td>
              <td>16/09/2018</td>
              <td>PENDING</td>
            </tr>
          </tbody>
        </Table>
      </Content>
    </Wrapper>
  );
}

const Wrapper = styled(Row)`
  height: 100%;
`;

const Content = styled(Box)`
  margin: 5%;
  padding: 5%;
  padding-top: 1%;
  width: 100%;
  flex-shrink: 1;
  margin-bottom: 15%;
  border: 4px solid #e7d2a1;
  color: #603157;
  line-height: 2;

  h1 {
    font-weight: lighter;
  }

  th {
    color: grey;
    font-weight: lighter;
  }

  .box {
    text-align: center;
    box-shadow: 0.5px 0.5px 0.5px 0.5px rgba(210, 215, 211, 1);
    margin-bottom: 10px;
  }

  table {
    width: 100%;
    border-collapse: separate;
    border-spacing: 10px;
  }
`;

export default withRouter(Invoice);
