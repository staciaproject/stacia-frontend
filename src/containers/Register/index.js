import { ErrorMessage, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Col, Row } from 'react-grid-system';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Box } from 'src/components/Box';
import Button from 'src/components/Button';
import { Label, TextInput } from 'src/components/Input';
import Footer from 'src/components/Footer';
import { register } from 'src/redux/auth';
import { media } from 'src/styles/theme';
import styled from 'styled-components';
import * as Yup from 'yup';

@connect(
  ({ auth, router }) => ({
    auth,
    router,
  }),
  {
    register,
  }
)
export default class Register extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    router: PropTypes.shape({
      location: PropTypes.shape({
        state: PropTypes.object,
      }).isRequired,
    }).isRequired,
    register: PropTypes.func.isRequired,
  };

  onSubmit = async (values, { setSubmitting, setErrors }) => {
    try {
      await this.props.register(values);
    } catch (e) {
      setErrors(this.props.auth.error);
    }
    setSubmitting(false);
  };

  schema = Yup.object().shape({
    email: Yup.string().required(),
    password: Yup.string()
      .min(6)
      .required(),
    confirmPassword: Yup.string()
      .required()
      .label('confirm password')
      .test('password-match', 'Passwords did not match.', function isMatch(val) {
        return this.parent.password === val;
      }),
    name: Yup.string().required(),
    phoneNumber: Yup.string()
      .label('phone number')
      .required(),
    birthday: Yup.string().required(),
    address: Yup.object()
      .shape({
        street: Yup.string()
          .label('address')
          .required(),
        country: Yup.string()
          .label('country')
          .required(),
        province: Yup.string()
          .label('province')
          .required(),
        city: Yup.string()
          .label('city')
          .required(),
        postalCode: Yup.string()
          .label('postal code')
          .required(),
      })
      .required(),
    isSubscribed: Yup.boolean().required(),
    acceptTos: Yup.boolean()
      .required()
      .test('is-true', 'Must agree to terms to continue.', value => value),
  });

  render() {
    const {
      auth,
      router: { location },
    } = this.props;

    // Redirects to home if already logged in
    if (auth.isAuthenticated) {
      const to = (location.state && location.state.from) || '/';
      return <Redirect to={to} />;
    }

    const initialValues = {
      email: '',
      password: '',
      confirmPassword: '',
      name: '',
      phoneNumber: '',
      birthday: '',
      address: {
        street: '',
        country: '',
        province: '',
        city: '',
        postalCode: '',
      },
      isSubscribed: false,
      acceptTos: false,
    };

    const RegisterForm = ({ values, handleChange }) => (
      <Box alignItems="flex-start" width="100%" maxWidth="40rem" className="form">
        <h2>new account</h2>
        <p>sign up with us and receive updates on our items and promotions</p>
        <FluidForm>
          <Label>
            e-mail
            <TextInput value={values.email} onChange={handleChange} name="email" type="email" />
            <ErrorMessage name="email" className="error" />
          </Label>
          <Label>
            password
            <TextInput
              value={values.password}
              onChange={handleChange}
              name="password"
              type="password"
            />
            <div className="error">
              <ErrorMessage name="password" />
            </div>
          </Label>
          <Label>
            confirm password
            <TextInput
              value={values.confirmPassword}
              onChange={handleChange}
              name="confirmPassword"
              type="password"
            />
            <div className="error">
              <ErrorMessage name="confirmPassword" />
            </div>
          </Label>
          <Label>
            name
            <TextInput value={values.name} onChange={handleChange} name="name" type="text" />
            <div className="error">
              <ErrorMessage name="name" />
            </div>
          </Label>
          <Label>
            mobile number
            <TextInput
              value={values.phoneNumber}
              onChange={handleChange}
              name="phoneNumber"
              type="text"
            />
            <div className="error">
              <ErrorMessage name="phoneNumber" />
            </div>
          </Label>
          <Label>
            birthdate
            <TextInput
              value={values.birthday}
              onChange={handleChange}
              name="birthday"
              type="date"
            />
            <div className="error">
              <ErrorMessage name="birthday" />
            </div>
          </Label>
          <Label>
            <h2>delivery address</h2>
          </Label>
          <Label>
            address
            <TextInput
              value={values.address.street}
              onChange={handleChange}
              name="address.street"
              type="text"
            />
            <div className="error">
              <ErrorMessage name="address.street" />
            </div>
          </Label>
          <Label>
            country
            <TextInput
              value={values.address.country}
              onChange={handleChange}
              name="address.country"
              type="text"
            />
            <div className="error">
              <ErrorMessage name="address.country" />
            </div>
          </Label>
          <Label>
            state/province
            <TextInput
              value={values.address.province}
              onChange={handleChange}
              name="address.province"
              type="text"
            />
            <div className="error">
              <ErrorMessage name="address.province" />
            </div>
          </Label>
          <Label>
            city/district
            <TextInput
              value={values.address.city}
              onChange={handleChange}
              name="address.city"
              type="text"
            />
            <div className="error">
              <ErrorMessage name="address.city" />
            </div>
          </Label>
          <Label>
            postal code
            <TextInput
              value={values.address.postalCode}
              onChange={handleChange}
              name="address.postalCode"
              type="text"
            />
            <div className="error">
              <ErrorMessage name="address.postalCode" />
            </div>
          </Label>
          <Label className="grey">
            <input
              type="checkbox"
              value={values.isSubscribed}
              onChange={handleChange}
              name="isSubscribed"
            />
            Keep me update with latest news and promotion
            <div className="error">
              <ErrorMessage name="isSubscribed" />
            </div>
          </Label>
          <Label className="grey">
            <input
              type="checkbox"
              value={values.acceptTos}
              onChange={handleChange}
              name="acceptTos"
            />
            I agree to Terms of Use and Privacy Policy
            <br />
            <div className="error">
              <ErrorMessage name="acceptTos" />
            </div>
          </Label>
          {this.props.auth.error && <div>{this.props.auth.error.non_field_errors}</div>}
          <Box padding="1rem 0" width="10rem">
            <Button primary size="m" type="submit">
              SIGN UP
            </Button>
          </Box>
        </FluidForm>
        <p>
          If you already have an account, please{' '}
          <Link to="/login">
            <b>log in</b>
          </Link>
        </p>
      </Box>
    );

    return (
      <Box width="100%">
        <RegisterContainer>
          <Row>
            <Box className="register-image-wrapper">
              <img className="register-image" src="/img/register.png" alt="register" />
            </Box>
            <Box className="register-form-wrapper">
              <img className="logo" src="/img/logo.png" alt="logo" />
              <Formik
                initialValues={initialValues}
                validationSchema={this.schema}
                onSubmit={this.onSubmit}
                render={RegisterForm}
              />
            </Box>
          </Row>
        </RegisterContainer>
        <Footer />
      </Box>
    );
  }
}

const RegisterContainer = styled(Box)`
  margin-bottom: 8%;
  justify-content: center;
  color: ${props => props.theme.colors.purple(0)};
  width: 100%;

  .register-image-wrapper {
    width: 50%;
    ${media('tabletPortrait')} {
      display: none;
      visibility: hidden;
    }
  }
  .register-image {
    width: 100%;
    height: 100%;
  }
  .register-form-wrapper {
    width: 50%;
    ${media('tabletPortrait')} {
      width: 100%;
    }
  }

  .form {
    border: 2px solid ${props => props.theme.colors.gold(0)};
    align-self: center;
    padding: 1.25rem;
    .error {
      color: ${props => props.theme.colors.red(0)};
    }
    label {
      display: inline-block;
      input {
        margin-right: 0.5rem;
      }
    }
  }

  .logo {
    width: 200px;
    height: auto;
    margin: 2rem 0;
  }
`;

const FluidForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;

  .grey {
    color: grey;
  }
`;
