import { MENU_TYPE } from 'src/common/constants';
import NotFound from './components/NotFound';
import UnderConstruction from './components/UnderConstruction';
import Home from './containers/Home';
import Login from './containers/Login';
import Logout from './containers/Logout';
import MakeupArtistry from './containers/MakeupArtistry';
import Article from './containers/Article';
import Products from './containers/Products';
import Checkout from './containers/Checkout';
import ArticleContent from './containers/ArticleContent';
import Shop from './containers/Shop';
import Invoice from './containers/Invoice';
import Profile from './containers/Profile';
import Cart from './containers/Cart';
import Register from './containers/Register';
import PaymentDetail from './containers/PaymentDetail';
import Details from './containers/Details';
import IconCart from '../public/img/icon-cart.svg';
import AdminLogin from './containers/AdminLogin';
import AdminRecoverPassword from './containers/AdminRecoverPassword';
import AdminHome from './containers/AdminHome';
import AdminInsertCatalog from './containers/AdminInsertCatalog';
import AdminInsertProduct from './containers/AdminInsertProduct';
import AdminInsertPortfolio from './containers/AdminInsertPortfolio';
import AdminProductDetails from './containers/AdminProductDetails';
import AdminOrder from './containers/AdminOrder';
import AdminEmail from './containers/AdminEmail';
import AdminBlog from './containers/AdminBlog';
import AdminList from './containers/AdminList';
import TransactionList from 'src/containers/TransactionList';
import Wishlist from 'src/containers/Wishlist';

export const publicRoutes = [
  {
    path: '/login',
    exact: true,
    component: Login,
  },
  {
    path: '/logout',
    exact: true,
    component: Logout,
  },
  {
    path: '/register',
    component: Register,
  },
  {
    path: '*',
    component: NotFound,
  },
];

export const privateRoutes = [
  {
    path: '/',
    title: 'HOME',
    type: MENU_TYPE.MAIN,
    component: Home,
    exact: true,
  },
  {
    path: '/adminemail',
    component: AdminEmail,
  },
  {
    path: '/adminblog',
    component: AdminBlog,
  },
  {
    path: '/adminhome',
    component: AdminHome,
  },
  {
    path: '/admin',
    component: AdminLogin,
  },
  {
    path: '/adminlist',
    component: AdminList,
  },
  {
    path: '/catalog',
    component: AdminInsertCatalog,
  },
  {
    path: '/product',
    component: AdminInsertProduct,
  },
  {
    path: '/productdetails/:id',
    component: AdminProductDetails,
  },
  {
    path: '/portfolio',
    component: AdminInsertPortfolio,
  },
  {
    path: '/adminorder',
    component: AdminOrder,
  },
  {
    path: '/recoverpassword',
    component: AdminRecoverPassword,
  },
  {
    path: '/makeup',
    title: 'MAKE UP ARTISTRY',
    type: MENU_TYPE.MAIN,
    component: MakeupArtistry,
  },
  // {
  //   path: '/eyelash/:id',
  //   component: Details,
  // },
  // {
  //   path: '/eyelash',
  //   title: 'STACIA EYELASH',
  //   type: MENU_TYPE.MAIN,
  //   component: Products,
  // },
  {
    path: '/tips/:id',
    component: ArticleContent,
  },
  {
    path: '/tips',
    title: 'BEAUTY TIPS',
    type: MENU_TYPE.MAIN,
    component: Article,
  },
  // {
  //   path: '/cart',
  //   title: 'Cart',
  //   type: MENU_TYPE.ICON,
  //   icon: IconCart,
  //   component: Cart,
  // },
  // {
  //   path: '/profile',
  //   title: 'Profile',
  //   type: MENU_TYPE.DROPDOWN,
  //   component: Profile,
  // },
  // {
  //   path: '/logout',
  //   title: 'Logout',
  //   type: MENU_TYPE.DROPDOWN,
  //   component: Logout,
  // },
  {
    path: '/shop',
    component: Shop,
  },
  // {
  //   path: '/invoice',
  //   component: Invoice,
  // },
  // {
  //   path: '/checkout',
  //   component: Checkout,
  // },
  // {
  //   path: '/paymentdetail',
  //   component: PaymentDetail,
  // },
  // {
  //   path: '/transactionlist',
  //   component: TransactionList,
  // },
  // {
  //   path: '/wishlist',
  //   component: Wishlist,
  // }
];
