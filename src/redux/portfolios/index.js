import { getPortfolios } from 'src/services/api';
import {
  asyncInitialState,
  createAsyncActions,
  createFetchThunk,
  createAsyncHandlers,
  createReducer,
} from 'src/redux/utils';

const resultsActions = status => createAsyncActions(`${status}/results`);
const activeActions = status => createAsyncActions(`${status}/active`);

const initialState = {
  results: { ...asyncInitialState, data: {} },
  active: { ...asyncInitialState, data: {} },
};

const handlers = {
  ...createAsyncHandlers(resultsActions('portfolios'), { path: ['results'] }),
};

export default createReducer(handlers, initialState);

export const loadPortfolios = createFetchThunk(resultsActions('portfolios'), getPortfolios);
