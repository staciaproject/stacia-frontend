import {
  createAsyncActions,
  createFetchThunk,
  createAsyncHandlers,
  createReducer,
} from 'src/redux/utils';

const statisticsActions = createAsyncActions('statistics');

const initialState = {
  bestSellers: {
    data: {},
  },
};

const handlers = {
  ...createAsyncHandlers(statisticsActions, { path: ['bestSellers'] }),
};

export default createReducer(handlers, initialState);
