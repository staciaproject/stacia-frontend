import instance, {
  login as apiLogin,
  getPermissions,
  register as apiRegister,
} from 'src/services/api';
import {
  createAction,
  createAsyncActions,
  createFetchThunk,
  asyncInitialState,
  createAsyncHandlers,
  createReducer,
} from 'src/redux/utils';

const loginActions = createAsyncActions('login');
const registerActions = createAsyncActions('register');
const setAuth = createAction('set_auth');
const resetAuth = createAction('reset_auth');
const setPermissions = createAction('set_permissions');

const initialState = {
  ...asyncInitialState,
  isAuthenticated: false,
  token: false,
};

const handlers = {
  ...createAsyncHandlers(loginActions),
  ...createAsyncHandlers(registerActions),
  [setAuth]: (state, payload) =>
    state
      .set('isAuthenticated', true)
      .set('isLoaded', true)
      .set('data', payload)
      .set('error', null),
  [resetAuth]: state =>
    state
      .set('isAuthenticated', false)
      .set('isLoaded', false)
      .set('data', null),
  [setPermissions]: (state, payload) => state.setIn(['data', 'user', 'permissions'], payload),
};

export default createReducer(handlers, initialState);

export const reloadPermissions = ({ token }) => async dispatch => {
  const { data } = await getPermissions(token);
  dispatch(setPermissions(data));
};

export const reloadAuth = data => dispatch => {
  const { token, user } = data;
  window.localStorage.setItem('token', token);
  window.localStorage.setItem('user', JSON.stringify(user));
  instance.defaults.headers.common.authorization = `Token ${token}`;
  dispatch(setAuth(data));
};

export const login = createFetchThunk(loginActions, apiLogin, {
  additional: (dispatch, getState) => {
    const { data } = getState().auth;
    dispatch(reloadAuth(data));
  },
});

export const register = createFetchThunk(registerActions, apiRegister);

export const logout = () => dispatch => {
  window.localStorage.removeItem('token');
  window.localStorage.removeItem('user');
  instance.defaults.headers.common.authorization = undefined;
  dispatch(resetAuth());
};
