import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import auth from './auth';
import home from './home';
import products from './products';
import articles from './articles';
import portfolios from './portfolios';

export default combineReducers({
  auth,
  home,
  products,
  articles,
  portfolios,
  router: routerReducer,
});
