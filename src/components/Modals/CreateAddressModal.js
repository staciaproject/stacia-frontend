import React from 'react';
import styled from 'styled-components';
import Popup from 'reactjs-popup';
import PropTypes from 'prop-types';
import { optionsType } from 'src/common/types';
import Button from 'src/components/Button';
import { Box, Row } from 'src/components/Box';
import CreateAddressForm from 'src/components/Forms/CreateAddressForm';
import { Colors } from 'src/styles/theme';

export default function CreateAddressModal(props) {
  const contentStyle = {
    overflowY: 'auto',
    overflowX: 'hidden',
    maxHeight: '80%',
    width: '50%',
    padding: 0,
  };

  return (
    <Popup
      trigger={<AddressButton>Add Address</AddressButton>}
      contentStyle={contentStyle}
      modal
      closeOnDocumentClick
    >
      {close => (
        <ModalContent>
          <CloseButton onClick={close}>&times;</CloseButton>
          <Row>
            <h2>new address</h2>
          </Row>
          <FormContent data={props.data} close={close} />
        </ModalContent>
      )}
    </Popup>
  );
}

CreateAddressModal.propTypes = {
  data: PropTypes.PropTypes.shape({
    purposes: optionsType,
    results: optionsType,
  }),
};

CreateAddressModal.defaultProps = {
  data: {
    purposes: [],
    results: [],
  },
};

export const ModalContent = styled(Box)`
  padding: 1rem 3.125rem;
  .icon-add {
    margin-right: 1rem;
  }
`;

const CloseButton = styled.button`
  cursor: pointer;
  position: absolute;
  right: 1rem;
  top: 1rem;
  font-size: 1rem;
  border-radius: 1.125rem;
`;

const FormContent = styled(CreateAddressForm)`
  overflow-y: auto;
  max-height: 80%;
  padding: 1rem 3.125rem;
`;

const AddressButton = styled(Button)`
  margin: 1rem auto;
  padding: auto 3rem;
  outline: none;
  border-radius: 25px;
  font-size: 10px;
  font-weight: 100;
`;
