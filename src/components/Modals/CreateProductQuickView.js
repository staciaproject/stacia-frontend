import PropTypes from 'prop-types';
import React from 'react';
import Popup from 'reactjs-popup';
import { Box, Col, Row } from 'src/components/Box';
import CreateAddressForm from 'src/components/Forms/CreateAddressForm';
import RatingBar from 'src/components/RatingBar';
import styled from 'styled-components';

export default function CreateAddressModal({
  img,
  sale,
  isLiked,
  likes,
  reviews,
  price,
  rating,
  name,
  sku,
  description,
  component,
}) {
  const contentStyle = {
    overflowY: 'auto',
    overflowX: 'hidden',
    maxHeight: '70%',
    width: '80%',
    padding: 0,
  };

  return (
    <Popup trigger={component} contentStyle={contentStyle} modal closeOnDocumentClick>
      {close => (
        <ModalContent>
          <CloseButton onClick={close}>&times;</CloseButton>
          <Row>
            <Col>
              <img src={img} alt="product_image" />
              <Row>
                <div className="small">
                  <RatingBar rating={rating} />
                </div>
                <div className="small">{reviews.length} review(s)</div>
                <div className="small">|</div>
                <div className="small">&#10084;</div>
                <div className="small">{likes} love(s)</div>
              </Row>
            </Col>
            <Col>
              <h2>{name}</h2>
              <p>SKU: {sku}</p>
              <content>{description} </content>
              <p className="details">PRODUCT DETAILS</p>
              <Row>
                <p className="price">IDR {price}</p>
              </Row>
            </Col>
          </Row>
        </ModalContent>
      )}
    </Popup>
  );
}

CreateAddressModal.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  img: PropTypes.string.isRequired,
  sale: PropTypes.bool.isRequired,
  isLiked: PropTypes.bool.isRequired,
  likes: PropTypes.number.isRequired,
  rating: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  reviews: PropTypes.array.isRequired,
  component: PropTypes.object.isRequired,
};

export const ModalContent = styled(Box)`
  color: #603157;
  margin: 0.5rem;
  border: 2px solid #e7d2a1;
  padding: 1rem 3.125rem;
  .icon-add {
    margin-right: 1rem;
  }
  h2 {
    margin-bottom: 0px;
  }
  p {
    margin-top: 0px;
    overflow: hidden;
  }
  content {
    width: 23%;
    line-height: 1rem;
    height: 30%;
    margin-bottom: 0px;
  }
  img {
    width: 20rem;
    height: 20rem;
    padding-right: 1rem;
  }

  .small {
    padding-right: 10px;
    padding-top: 10px;
  }

  .details {
    font-weight: bold;
    padding-top: 10px;
    padding-left: 18%;
  }

  .price {
    font-size: 25px;
    position: absolute;
    bottom: 0%;
  }

  .qty {
    color: grey;
    position: absolute;
    bottom: 0%;
  }
`;

const CloseButton = styled.button`
  cursor: pointer;
  position: absolute;
  right: 1rem;
  top: 1rem;
  font-size: 1rem;
  border-radius: 1.125rem;
  border-color: #ffffff;
`;

const FormContent = styled(CreateAddressForm)`
  overflow-y: auto;
  max-height: 80%;
  padding: 1rem 3.125rem;
`;
