import styled from 'styled-components';
import { sizes, shadows } from '../styles/theme';
import { Box } from './Box';

const Container = styled(Box)`
  border-radius: ${sizes.radius.reg};
  box-shadow: ${shadows[0]};
  flex-shrink: 0;
  ${props =>
    props.accent &&
    `
    border-left: ${sizes.border.xl} solid ${props.accent};
  `};
`;

const Content = styled(Box)`
  padding: ${sizes.spacing.regular} ${sizes.spacing.medium};
`;

const Card = {
  Container,
  Content,
};

/** @component */
export default Card;
