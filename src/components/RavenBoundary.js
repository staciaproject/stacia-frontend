import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Raven from 'raven-js';

import Box from './Box';
import Button from './Button';

export default class RavenBoundary extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
    getState: PropTypes.func.isRequired,
  };

  state = {
    error: null,
  };

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
    Raven.captureException(error, { state: this.props.getState(), extra: errorInfo });
  }

  render() {
    if (this.state.error) {
      return (
        <Box height="100%" alignItems="center" justifyContent="center">
          <h3>We&apos;re sorry — something&apos;s gone wrong.</h3>
          <p>
            Error: `{this.state.error.message}`. Our team has been notified, but click below to fill
            out a report.
          </p>
          <Button onClick={() => Raven.lastEventId() && Raven.showReportDialog()}>Report</Button>
        </Box>
      );
    }

    return this.props.children;
  }
}
