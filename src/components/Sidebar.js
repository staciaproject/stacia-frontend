import React from 'react';
import styled from 'styled-components';
import { withRouter, NavLink } from 'react-router-dom';
import { Box } from './Box';

function Sidebar() {
  return (
    <Bar>
      <GlowingLine />
      <SidebarItem to='/eyelash'>
        <div>PRODUCT</div>
        <ul>
          <li>SR</li>
          <li>SM</li>
        </ul>
      </SidebarItem>
      <SidebarItem to='/best-seller'>
        <div>BEST SELLER</div>
      </SidebarItem>
      <SidebarItem to='/sale'>
        <div>SALE</div>
      </SidebarItem>
    </Bar>
  );
}

const Bar = styled.aside`
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  width: 14rem;
  padding: 2rem 3rem;
  background-image: url("/img/articles/article8.png");
  background-repeat: repeat-y;
  background-size: 100% auto;
`;

const SidebarItem = styled(NavLink)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  text-decoration: none;
  li {
    list-style: none;
  }
`;

const GlowingLine = styled(Box)`
  height: 5px;
  width: 100%;
  margin: 0.5rem 0;
  background-color: #eeb124;
  background-image: linear-gradient(to right, #e7d2a1, #faf8cb, #e7d2a1, #faf8cb, #e7d2a1, #faf8cb, #e7d2a1, #faf8cb);

  h1 {
    text-align: center;
    vertical-align: middle;
    line-height: 60px;   
    color: #603157;
    font-size: 30px;
    font-weight: bold;
    font-family: Raleway;
  }
`;

export default withRouter(Sidebar);
