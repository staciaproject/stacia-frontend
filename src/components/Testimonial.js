import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import styled from 'styled-components';
import Box from './Box';
import { Row } from 'react-grid-system';
import { loadArticles } from 'src/redux/articles';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Loader from 'src/components/Loader';

const Testimonial1 = "Bulumata Favorite sih ini, bahannya ringan gak nusuk-nusuk, harganya worth it dengan kualitas kaya gini.. Wajib Repurchase kalo stock dirumah abis.. Luuuuvvvv..."
const Testimonial2 = "Stacia Eyelash bulumatanya halus, gampang pakenya, suka banget deh.. "
const Testimonial3 = "Awal nyoba langsung suka, apalagi SR01 favorite banget sih buat hari-hari biasa.. Seharian pake gak kerasa sama sekali, bulunya juga halus. Recomend banget sih buat kalian yang suka pake bulumata"


@connect(
  state => ({
    results: state.articles.results,
  }),
  { loadArticles }
)
class Testimonial extends Component {
    static propTypes = {
      results: PropTypes.object.isRequired,
      loadArticles: PropTypes.func.isRequired,
    };

    componentDidMount() {
      this.props.loadArticles();
    }

    render() {
      const {
        results: { data, isLoading, error, isLoaded },
      } = this.props;

      if (!isLoaded || isLoading) return <Loading />;
      if (error) return <Message text={`Error: ${error}`} />;

      return (
        <Content> 
          <SeparatorLine />
          <h1 className="testimonial">T E S T I M O N I A L</h1>
          <Table responsive>
            <thead>
              <tr>
                <th><img src="/img/quote.png"></img></th>
                <th><img src="/img/quote.png"></img></th>
                <th><img src="/img/quote.png"></img></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                {data.slice(0).map((testimonial, index) => (
                  <td>{testimonial.text}</td>
                ))
                }
              </tr>
              <tr>
                {data.slice(0).map((testimonial, index) => (
                  <td style={{fontWeight:'bold'}}>{testimonial.title}</td>
                ))
                }
              </tr>
            </tbody>
          </Table>
        </Content>
      );
    }
};

const Loading = () => (
    <Content> 
      <SeparatorLine />
      <h1 className="testimonial">T E S T I M O N I A L</h1>
      <Table responsive>
        <thead>
          <tr>
            <Centered>
              <Loader size={10} />
            </Centered>
          </tr>
        </thead>

      </Table>
    </Content>
);

const Centered = styled(Row)`
  width: 10%;
  justify-content: center;
  align-items:center;
`;

const Content = styled(Box)`
  align-items: center;
  padding-bottom: 50px;
  h1 {
    font-weight: normal;
    font-family: Raleway;
    color: #603157;
  }

  Table {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    color: #603157;
  }

  th {
    text-align: center;
  }

  .table>tbody>tr>td {
    border-top: none;
    padding-left: 15px;
    padding-right: 15px;
  }

  .name {
    font-weight: bold;
    font-family: Raleway;
  }

  img {
    height:20px;
  }

  @media (max-width: 480px) {
    h1 {
      font-size: 15px;
    }

    font-size: 10px;
    img {
      height:10px;
    }
  }
`;


const SeparatorLine = styled.hr`
  height: 2px;
  width: 5%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 50px;
  border: 0;
  background-color: #603157; 
`;

export default withRouter(Testimonial);
