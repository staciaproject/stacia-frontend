import React, { Component } from 'react';
import styled from 'styled-components';
import { Box, Row, Col } from './Box';

import ProductImage1 from '../../public/img/products/SR04_02.jpg';



export default class RatingAndReviews extends Component {
    render() {
        return (
        <Wrapper>
            <Col className="center"> 
                <h1>Rating and Reviews</h1>
                <Review>
                    <Row>
                        <img className="review-profile" src={ProductImage1}/>
                        <h3 className="review-name">Kendal Jenner</h3>
                        <Col className="review-content">
                            <div className="small">&#9733; &#9733; &#9734; &#9734; &#9734;</div>
                            <h2>Just buy it!</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua.</p>
                        </Col>
                    </Row>
                </Review>
                <Review>
                    <Row>
                        <img className="review-profile" src={ProductImage1}/>
                        <h3 className="review-name">Kendal Jenner</h3>
                        <Col className="review-content">
                            <div className="small">&#9733; &#9733; &#9734; &#9734; &#9734;</div>
                            <h2>Just buy it!</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua.</p>
                        </Col>
                    </Row>
                </Review>
                <Review>
                    <Row>
                        <img className="review-profile" src={ProductImage1}/>
                        <h3 className="review-name">Kendal Jenner</h3>
                        <Col className="review-content">
                            <div className="small">&#9733; &#9733; &#9734; &#9734; &#9734;</div>
                            <h2>Just buy it!</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua.</p>
                        </Col>
                    </Row>
                </Review>
            </Col>
        </Wrapper>
        );
    }
}


const Wrapper = styled(Box)`
  margin: 1rem;
  width: 100%;
  color: #603157;
  .center {
      align-items: center;
  }
`;

const Review = styled(Box)`
    border-top: 1px solid grey;
    border-bottom: 1px solid grey;
    justify-content: center;
    padding-top: 1rem;
    padding-bottom: 1rem;

    .review-profile{
        height: 90px;
        width: 90px;
        border-radius: 50px;
        border: 2px solid grey;
        margin-right: 1rem;
    }

    .review-name {
        margin-right: 15%;
        color: #603157;
    }

    .review-content {
        width: 60%;
    }
`;

