import styled from 'styled-components';

import Box from './Box';

const PageHeader = styled.header`
  padding: 1rem 2rem;
  box-shadow: ${props => props.theme.shadows[1]};
`;

PageHeader.Title = styled.h1`
  font-size: 2rem;
  color: ${props => props.theme.colors.darkBlue};
`;

PageHeader.FilterRow = styled(Box)`
  flex-direction: row;
  align-items: center;
  border: 1px solid ${props => props.theme.colors.grey(3)};
  padding: 0.25rem 0 0.25rem 1rem;
  border-radius: ${props => props.theme.sizes.radius.reg};
`;

export default PageHeader;
