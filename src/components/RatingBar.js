import { range } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

export default function RatingBar({ rating, maxRating }) {
  return (
    <React.Fragment>
      {range(rating).map(i => (
        <i className="fas fa-star" key={`rating${i}`} />
      ))}
      {range(maxRating - rating).map(i => (
        <i className="far fa-star" key={`rating${maxRating - i}`} />
      ))}
    </React.Fragment>
  );
}

RatingBar.propTypes = {
  rating: PropTypes.number.isRequired,
  maxRating: PropTypes.number,
};

RatingBar.defaultProps = {
  maxRating: 5,
};
