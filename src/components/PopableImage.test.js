import React from 'react';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';

import PopableImage from './PopableImage';

afterEach(() => {
  jest.useFakeTimers();
  jest.runAllTimers();
});

test('PopableImage', () => {
  jest.mock('aphrodite/lib/inject');
  const wrapper = mount(<PopableImage src="https://example.com" alt="example" />);
  expect(toJSON(wrapper)).toMatchSnapshot();
  wrapper.simulate('click');
  expect(toJSON(wrapper)).toMatchSnapshot();
});
