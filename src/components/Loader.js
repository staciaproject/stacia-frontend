import React from 'react';
import PropTypes from 'prop-types';
import { colors } from '../styles/theme';
import styled from 'styled-components';

const LoaderWrapper = styled.div`
  display: inline-block;
  position: relative;
  width: ${props => props.size}rem;
  height: ${props => props.size}rem;
`;

const Ripple = styled.div`
  position: absolute;
  border: ${props => props.size / 16}rem solid ${props => props.color};
  opacity: 1;
  border-radius: 50%;
  animation: ${({ size }) => `loader-ripple-${size} 1s cubic-bezier(0, 0.2, 0.8, 1) infinite`};
  ${props => props.delay && `animation-delay: ${props.delay}`};

  @keyframes ${({ size }) => `loader-ripple-${size}`} {
    0% {
      top: ${props => props.size * 0.4375}rem;
      left: ${props => props.size * 0.4375}rem;
      width: 0;
      height: 0;
      opacity: 1;
    }
    100% {
      top: 0;
      left: 0;
      width: ${props => props.size * 0.875}rem;
      height: ${props => props.size * 0.875}rem;
      opacity: 0;
    }
  }
`;

const Loader = ({ size = 4, color = colors.grey() }) => (
  <LoaderWrapper size={size}>
    <Ripple size={size} color={color} />
    <Ripple size={size} color={color} delay="-0.5s" />
  </LoaderWrapper>
);

Loader.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

/** @component */
export default Loader;
