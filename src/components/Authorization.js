import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import NotAuthorized from 'src/components/NotAuthorized';

const Authorization = (WrappedComponent, allowedPermissions) => {
  @connect(state => ({
    permissions: state.auth.data.user.permissions,
  }))
  class WithAuthorization extends Component {
    static propTypes = {
      permissions: PropTypes.array.isRequired,
    };

    render() {
      const { permissions } = this.props;
      const permitted = allowedPermissions.some(permit => permissions.includes(permit));
      if (permitted) {
        return <WrappedComponent {...this.props} />;
      }
      return <NotAuthorized />;
    }
  }
  return WithAuthorization;
};

export default Authorization;
