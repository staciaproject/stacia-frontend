import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import styled from 'styled-components';
import { Box } from './Box';
import { Link } from 'react-router'

import ProductImage1 from '../../public/img/products/SL01_02.jpg';
import ProductImage2 from '../../public/img/products/SM01_02.jpg';
import ProductImage3 from '../../public/img/products/SM02_02.jpg';
import ProductImage4 from '../../public/img/products/SM03_02.jpg';
import ProductImage5 from '../../public/img/products/SR01_02.jpg';
import ProductImage6 from '../../public/img/products/SR02_02.jpg';
import ProductImage7 from '../../public/img/products/SR03_02.jpg';
import ProductImage8 from '../../public/img/products/SR04_02.jpg';

class EyelashCarousel extends Component {
  onSlideChange(e) {}

  onSlideChanged(e) {}

  responsive = {
    0: { items: 1 },
    600: { items: 2 },
    1024: { items: 3 },
  };

  galleryItems() {
    const items = [
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage2} alt="SM01_02" />
        </a>
        <div className="gallery-item-text">
          <h2>SM-01</h2>
          <p><b>IDR 20.000</b></p>
          <p>Seri SM01 ini adalah signature lashes Stacia Additional Lashes dengan kelentikan maksimal yang memberi kesan mata lebih besar dan dramatis hanya dengan 1 lapis bulumata namun tidak terkesan berat. Motif saling silangnya cocok digunakan untuk pengguna yang memiliki mata kecil. Membuat mata terlihat lebih besar.</p>
        </div>
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage3} alt="SM02_02" />
        </a> 
        <div className="gallery-item-text">
          <h2>SM-02</h2>
          <p><b>IDR 20.000</b></p>
          <p>Seri SM02 merupakan seri 3D pertama dari Stacia Additional Lashes. Tampilan 3 dimensinya memberikan efek BOLD pada riasan mata Anda namun tidak terlihat berat, bahkan tidak terasa berat di mata pula. Tampilannya mirip dengan hasil eyelash extension.</p>
        </div>    
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage4} alt="SM03_02" />
        </a>
        <div className="gallery-item-text">
          <h2>SM-03</h2>
          <p><b>IDR 20.000</b></p>
          <p>Bulumata SM03 memiliki kelentikan maksimal dan helaian rambut yang panjang namun tidak terlalu tebal. Cocok digunakan untuk Anda yang memiliki mata besar sehingga terlihat lebih cantik, dapat juga dijadikan kombinasi untuk makeup pengantin agar terlihat lebih dramatis namun tetap lentik nan cantik.</p>
        </div>
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage5} alt="SR01_02" />
        </a>
        <div className="gallery-item-text">
          <h2>SR-01</h2>
          <p><b>IDR 15.000</b></p>
          <p>Seri Stacia Additional Lashes yang paling natural dan ringan, hampir tidak terasa ketika dikenakan di mata. Dapat digunakan sehari-hari atau menjadi lapisan pertama yang sempurna untuk jahit mata atau sebagai dasar pertama tumpukan bulumata pada makeup lamaran, pernikahan, dll. </p>
        </div>
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage6} alt="SR02_02" />
        </a>
        <div className="gallery-item-text">
          <h2>SR-02</h2>
          <p><b>IDR 15.000</b></p>
          <p>Seri SR02 adalah tipe yang mudah digunakan untuk pergi ke acara-acara semi formal atau untuk kebutuhan sehari-hari. Tampilannya yang natural menambahkan kecantikan pada mata Anda, tanpa terkesan berat dan palsu. Anda dapat juga menggunakan SR02 sebagai bulumata lapisan pertama untuk dikombinasikan dengan seri lain pada makeup acara-acara penting.</p>
        </div>
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage7} alt="SR03_02" />
        </a>
        <div className="gallery-item-text">
          <h2>SR-03</h2>
          <p><b>IDR 15.000</b></p>
          <p>Bagi Anda yang menyukai makeup semi bold, SR03 bisa menjadi pilihan yang tepat. Motifnya yang cukup tebal dan terkesan runcing, memberikan efek dramatis pada mata. 1 lapis saja dan mata Anda akan terlihat lebih besar dan hidup.</p>
        </div>
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage8} alt="SR04_02" />        
        </a>
        <div className="gallery-item-text">
          <h2>SR-04</h2>
          <p><b>IDR 15.000</b></p>
          <p>Tampilan whispy bulumata SR04 yang terlihat ringan dan natural ini memberikan kesan lentik pada bentuk mata apapun. Dapat digunakan ke pesta namun juga tidak terlalu berat untuk digunakan sehari-hari.</p>
        </div>
      </div>,
      <div className="gallery-item-wrapper">
        <a href="/shop">
          <img className="gallery-item" src={ProductImage1} alt="SL01_02" />
        </a>
        <div className="gallery-item-text">
          <h2>SL-01</h2>
          <p><b>IDR 15.000</b></p>
          <p>Seri pertama bulumata bawah Stacia Additional Lashes. Tampilannya yang natural dengan kombinasi motif yang beragam membuat SL01 menjadi salah satu bulumata bawah yang paling natural namun membuat mata terlihat sangat cantik.</p>
        </div>
      </div>,
    ];

    return [0, 1, 2, 3, 4, 5, 6, 7].map((item, i) => (
      <div key={`key-${i}`} className="gallery-items">
        <div style={{ height: '100px' }}>{items[item]}</div>
      </div>
    ));
  }

  render() {
    const items = this.galleryItems();

    return (
      <Content>
        <Title>
          <h1>EYELASHES</h1>
        </Title>
        <hr className="line" />
        <div className="carousel">
          <AliceCarousel
            items={items}
            duration={400}
            startIndex={0}
            fadeOutAnimation
            autoPlayInterval={2000}
            disableAutoPlayOnAction
            onSlideChange={this.onSlideChange}
            onSlideChanged={this.onSlideChanged}
            
          />
        </div>
        <hr className="line" />
      </Content>
    );
  }
}

const Content = styled(Box)`
  .line {
    height: 5px;
    width: 70%;
    border-top: transparent;
    border-left: transparent;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 10px;
    background-color: #e7d2a1;
    background-image: linear-gradient(
      to right,
      #e7d2a1,
      #faf8cb,
      #e7d2a1,
      #faf8cb,
      #e7d2a1,
      #faf8cb,
      #e7d2a1,
      #faf8cb
    );
  }

  .carousel {
    width: 70%;
    margin-left: auto;
    margin-right: auto;
  }

  .slide {
    width: 100%;
    margin-left: 0px;
    margin-right: 0px;
  }

  .carousel-control {
    color: #603157;
    width: 5%;
    background: none !important;
    opacity: 1;
  }

  .gallery-items {
    height: 250px;
  }

  .gallery-item-text {
    color: #603157;
    margin-left: 1rem;
    padding-right: 3rem;
  }

  .gallery-item-wrapper {
    height: 250px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    display: flex;
    flex-direction: row;
  }

  .gallery-item {
    height: 250px;
    padding-left: 3rem;
    margin-top: 1rem;
  }

  .view-button {
    margin-top: 5px;
    color: #603157;
    border-color: #603157;
    background-color: transparent;
    border-style: solid;
    padding: 5px;
    border-width: 2px;
    text-decoration: None;
  }

  @media(max-width:480px){

    .gallery-items {
      height: 370px;
      padding-right: 1rem;
      padding-left: 1rem;
    }
  
    .gallery-item-wrapper {
      height: 150px;
      display: flex;
      align-items: center;
      justify-content: space-between;
      display: flex;
      flex-direction: column;
    }

    .gallery-item {
      height: 100px;
      margin-top: 1rem;
      padding-left: 0;
    }

    .gallery-item-text {
      color: #603157;
      text-align: center;
      padding-right: 0rem;
      padding-left: 0rem;
    
      h2 {
        font-size: 15px;
      }
    }
    font-size: 12px;
      
    }

    .view-button {
      margin-top: 5px;
      color: #603157;
      border-color: #603157;
      background-color: transparent;
      border-style: solid;
      padding: 5px;
      border-width: 2px;
      text-decoration: None;
      font-size:12px;
    }
  }
`;

const Title = styled(Box)`
  align-items: center;
  justify-content: center;
  height: 60px;
  width: 70%;
  margin: 1rem auto;
  background-color: #e7d2a1;
  background-image: linear-gradient(
    to right,
    #e7d2a1,
    #faf8cb,
    #e7d2a1,
    #faf8cb,
    #e7d2a1,
    #faf8cb,
    #e7d2a1,
    #faf8cb
  );

  h1 {
    text-align: center;
    vertical-align: middle;
    line-height: 60px;
    color: #603157;
    font-size: 30px;
    font-weight: bold;
    font-family: Raleway;
  }

  @media(max-width:480px){
    height:30px;
    margin-bottom: 0rem;
    h1 {
      font-size:15px;
    }
  }
`;

export default EyelashCarousel;
