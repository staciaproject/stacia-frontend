import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box, Row } from './Box';

import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';

export default class ProductDetails extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    sale: PropTypes.bool.isRequired,
    liked: PropTypes.bool.isRequired,
    rating: PropTypes.number.isRequired,
  };

  render() {
    const { img, sale, liked, price, rating, id } = this.props;
    return (
      <Wrapper>
        <ProductImage>
            <img src={img} alt="product_image"/>
            <CreateProductQuickView/>    
        </ProductImage>
        <ProductInfo>
          <Row justifyContent="space-between" width="18rem">
            <div className="text">{id}</div>
            <div className="text">&#10084;</div>
          </Row>
          <Row justifyContent="space-between" width="18rem">
            <div className="text">{price}</div>
            <div className="text">&#9733; &#9733; &#9734; &#9734; &#9734;</div>
          </Row>
        </ProductInfo>
      </Wrapper>
    );
  }
}


const Wrapper = styled(Box)`
  margin: 1rem;
  flex: 1 0 21%;
`;

const ProductInfo = styled(Box)`
  margin: 10px 0;
  width: 20rem;
  height: 5rem;
  justify-content: space-between;
  padding: 1rem;
  background-color: ${props => props.theme.colors.grey(4)};
  .text {
    color: ${props => props.theme.colors.purple(0)};
  }
`;

const ProductImage = styled(Box)`
  width: 20rem;
  height: 20rem;
  overflow: hidden;
  align-items: center;
  justify-content: center;
  position: relative;
`;


