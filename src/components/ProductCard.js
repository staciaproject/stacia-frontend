import PropTypes from 'prop-types';
import React from 'react';
import Button from 'src/components/Button';
import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';
import RatingBar from 'src/components/RatingBar';
import styled from 'styled-components';
import { Box, Row } from './Box';

export default function ProductCard(props) {
  const { img, sale, isLiked, likes, price, rating, name, description, sku, reviews } = props;

  return (
    <Wrapper>
      <ProductImage>
        <img src={img} alt="product_image" />
        <CreateProductQuickView component={<ViewButton>QUICK LOOK</ViewButton>} {...props} />
      </ProductImage>
      <ProductInfo>
        <Row justifyContent="space-between" width="18rem">
          <div className="text">{name}</div>
          <div className="text">
            {likes} <i className="fas fa-heart" />
          </div>
        </Row>
        <Row justifyContent="space-between" width="18rem">
          <div className="text">IDR {price}</div>
          <div className="text">
            <RatingBar rating={rating} />
          </div>
        </Row>
      </ProductInfo>
    </Wrapper>
  );
}

ProductCard.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  img: PropTypes.string.isRequired,
  sale: PropTypes.bool.isRequired,
  isLiked: PropTypes.bool.isRequired,
  likes: PropTypes.number.isRequired,
  rating: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  reviews: PropTypes.array.isRequired,
};

const Wrapper = styled(Box)`
  margin: 1rem;
  flex: 1 0 21%;
`;

const ProductInfo = styled(Box)`
  margin: 10px 0;
  width: 20rem;
  height: 5rem;
  justify-content: space-between;
  padding: 1rem;
  background-color: ${props => props.theme.colors.grey(4)};
  .text {
    color: ${props => props.theme.colors.purple(0)};
  }
`;

const ProductImage = styled(Box)`
  width: 20rem;
  height: 20rem;
  overflow: hidden;
  align-items: center;
  justify-content: center;
  position: relative;
`;

const ViewButton = styled(Button)`
  outline: none;
  font-size: 16px;
  font-weight: 100;
  display: block;
  width: 100%;
  position: absolute;
  top: 90%;
  border-radius: 0px;
`;
