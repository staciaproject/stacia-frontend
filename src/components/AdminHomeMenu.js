import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box, Row } from './Box';
import { withRouter, NavLink } from 'react-router-dom';

import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';

export default class AdminHomeMenu extends Component {
  static propTypes = {
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
  };

  render() {
    const { icon, title, to } = this.props;
    return (
      <Wrapper>
        <MenuImage to={to}>
            <h2>{title}</h2>
            <i class={icon}></i>  
        </MenuImage>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Box)`
  margin: 1rem;
  flex: 1 0 21%;
`;


const MenuImage = styled(NavLink)`
  width: 20rem;
  height: 10rem;

  position: relative;
  background: #562b4f;

  h2 {
      color: white;
      position: absolute;
      right: 0.5rem;
      top: 0.2rem;
      margin-block-start:0.5rem;
  }

  i {
    font-size: 5em;
    left: 1rem;
    position: absolute;
    top: 4rem;
    color: white;
  }
`;
