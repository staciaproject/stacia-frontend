import styled from 'styled-components';
import { Box } from 'src/components/Box';

export const ContentCard = styled(Box)`
  min-width: 100%:
  justify-content: center;
  padding: 2rem 4rem;
  @media (max-width: 480px) {
    padding: 0.5rem 1rem;
  }
  flex-wrap: wrap;
  ${props => props.backgroundImage && `background-image: ${props.backgroundImage}`};
  // ${props => props.alignItems && `align-items: ${props.alignItems}`};
  // ${props => props.justifyContent && `justify-content: ${props.justifyContent}`};
`;

/** @component */
export default ContentCard;
