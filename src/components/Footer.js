import React from 'react';
import styled from 'styled-components';
import { Row } from './Box';

function Footer() {
  return (
    <Content>
      <FooterLine />
      <FooterItems>
        <div />
        <p>Copyrights &copy; 2020. All Rights, Reserved</p>
        <div className="icons">
          <div className="icon-wrapper">
            <a href=" https://www.facebook.com/StaciaMakeupArtistry/">
              <img className="icon" src="/img/facebook.png" alt="icon-facebook" />
            </a>
          </div>
          <div className="icon-wrapper">
            <a href="https://instagram.com/staciamakeup/">
              <img className="icon" src="/img/instagram.png" alt="icon-instagram" />
            </a>
          </div>
          <div className="icon-wrapper">
            <a href="https://api.whatsapp.com/send?phone=+628112209966">
              <img className="icon" src="/img/wa.png" alt="icon-whatsapp" />
            </a>
          </div>
          <div className="icon-wrapper">
            <a href="https://www.youtube.com/channel/UCm4f0IZs-W0SYZnpbDzkCYw">
              <img className="icon" src="/img/youtube.png" alt="icon-youtube" />
            </a>
          </div>
        </div>
      </FooterItems>
    </Content>
  );
}

const Content = styled.footer`
  background-color: #693d61;
  bottom: 0;
  width: 100%;
  padding: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;

  .icon-wrapper {
    display: inline-block;
    margin-right: 5px;
  }

  .icons {
    text-align:center;
  }

  .icon {
    height: 25px;
    margin: 5px;
  }

  @media (max-width: 480px) {
    .icons {
      text-align:center;
    }

    .icon-wrapper {
      display: inline-block;
      margin-right: 0px;
    }

    .icon {
      height: 10px;
      margin: 2px;
    }
  }
`;

const FooterItems = styled(Row)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90%;
  p {
    color: ${props => props.theme.colors.white};
  }

  @media (max-width: 480px) {
    p {
      font-size: 8px;
    }
  }
`;

const FooterLine = styled.hr`
  height: 5px;
  border: 0;
  width: 90%;
  margin-left: auto;
  margin-right: auto;
  background-color: #e7d2a1;
`;

export default Footer;
