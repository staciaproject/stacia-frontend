import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from './Box';
import { Container, Row, Col } from 'react-bootstrap';
import { withRouter, NavLink } from 'react-router-dom';
import { Table } from 'react-bootstrap';

import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';

import ProductImage1 from '../../public/img/eyelash_05.png';

export default class AdminProduct extends Component {
  static propTypes = {
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
  };

  render() {
    const { icon, title, to } = this.props;
    return (
      <Wrapper>
        <CatalogContent>
          <Table style={{border: '1px solid #dadbdb'}}>
              <tr>
                <td colSpan={4} style={{backgroundColor:'#dadbdb',paddingTop:'0',paddingBottom:'0'}}>
                  <h4 style={{color:'grey', marginTop:'1rem', marginBottom:'1rem'}}>{title}</h4>
                </td>
              </tr>
              <tr>
                <td colSpan={4} >
                  <p>Product Photo</p>
                  <input type="checkbox" style={{height:'100px'}}/>      
                  <img src={ProductImage1} />
                  <input type="checkbox" style={{height:'100px'}}/>      
                  <img src={ProductImage1} />
                  <input type="checkbox" style={{height:'100px'}}/>      
                  <img src={ProductImage1} />
                </td>
              </tr>
              <tr>
                <td colSpan={2} >
                  <p>Nama Product</p>
                  <input type="type" />   
                </td>
                <td colSpan={2}>
                  <p>Etalase</p>
                  <input type="type" /> 
                </td>
              </tr>
              <tr>
                <td>
                  <p>Pengaturan Harga</p>
                  <p>Rp <input type="type" /> </p>  
                </td>
                <td>
                  <p>Stock</p>
                  <input type="type" />
                </td>
                <td>
                  <p>Single Description</p>
                  <input type="type" />
                </td>
                <td>
                  <div className="text">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td colSpan={4} >
                  <p>Deskripsi Produk</p>
                  <textarea type="type" />
                </td>
              </tr>
              <tr>
                <td colSpan={4} >
                  <p>How to Use</p>
                  <textarea type="type" />
                </td>
              </tr>
              <tr>
                <td colSpan={8} style={{textAlign:'right'}}>
                  <button>Batal</button>
                  <button style={{marginRight:'2rem'}}>Simpan</button>
                </td>
              </tr>
          </Table>

          
        </CatalogContent>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Box)`
  margin: 1rem;
  flex: 1 0 100%;
  margin:0px;
  margin:auto;
  margin-top:1rem;
  margin-bottom:1rem;
  border: 1px solid #dadbdb;
`;


const CatalogContent = styled(Box)`
  width: 65rem;
  height: auto;

  position: relative;
  background: transparent;

  h4 {
    font-size: 20px;
    margin-top: 1rem;
    margin-left: 1rem;
    margin-bottom:5px;
  }

  td {
    padding: 1rem;

  }

  i {
    color:grey;
  }

  p {
    margin-top: 0px;
    margin-left: 1rem;
    font-family: Raleway;
    color: grey;
    font-size: 14px;
    font-weight: bold;
  }

  img {
    height: 100px;
    width: auto;
    padding-left:1rem;
    padding-right:1.8rem;
  }

  input {
    margin-left: 1rem;
  }

  textarea {
    border: 1px solid #dadbdb;
    width:95%;
    margin-right:1rem;
    margin-left:1rem;
    height:5rem
  }

`;
