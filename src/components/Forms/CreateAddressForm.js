import React, { Component } from 'react';
import styled from 'styled-components';
import { Formik, Form, Field, FieldArray } from 'formik';
import { prettifySnakeCase } from 'src/common/utils';
import { Box } from 'src/components/Box';
import { Label } from 'src/components/Input';
import Button from 'src/components/Button';

export default class CreateAddressForm extends Component {
  onSubmit = () => {
    console.log('DEBUG onSubmit called');
  };
  render() {
    const values = {
      address: '',
      country: '',
      province: '',
      city: '',
      postalCode: '',
    };
    return (
      <Formik
        initialValues={values}
        onSubmit={this.onSubmit}
        render={formProps => (
          <Form>
            <FieldArray
              render={() => (
                <Box>
                  {Object.entries(formProps.values).map(([key, value]) => (
                    <Entry className="entry" key={key}>
                      <Label>
                        {prettifySnakeCase(key)}
                        <Field className="value" name={key} value={value === 0 ? 0 : value || ''} />
                      </Label>
                    </Entry>
                  ))}
                  <Box alignItems="center" padding="0.5rem 0">
                    <Button type="submit" onClick={() => formProps.onSubmit}>
                      Save
                    </Button>
                  </Box>
                </Box>
              )}
            />
          </Form>
        )}
      />
    );
  }
}

const Entry = styled(Box)`
  .value {
    padding: 0.625rem;
    width: 100%;
  }
`;
