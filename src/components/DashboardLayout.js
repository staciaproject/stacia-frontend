import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { NavLink, withRouter } from 'react-router-dom';
import Footer from 'src/components/Footer';
import AdminNavbar from './AdminNavbar';
import UserNavbar from './UserNavbar';
import { Box } from './Box';


@withRouter
export default class DashboardLayout extends React.Component {

  static propTypes = {
    children: PropTypes.node.isRequired,
    isAdmin: PropTypes.bool.isRequired,
  };

  render() {
    const { children, isAdmin } = this.props;
    // alert(isAdmin)
    if (isAdmin) {
      return (
        <Box width="100%" height="100%" justifyContent="stretch" scroll="hidden">
        <Content flex={1}>
            <AdminNavbar />
            {children}
        </Content>
        <Footer />
        </Box>
      );
    }
    else {
      return (
        <Box width="100%" height="100%" justifyContent="stretch" scroll="hidden">
        <Content flex={1}>
            <UserNavbar />
            {children}
        </Content>
        <Footer />
        </Box>
      );
    }
  }
}


const Content = styled.main`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 6rem;
  width: 100%;
  background: ${props => props.theme.colors.grey(6)};

  h3,
  h4 {
    color: ${props => props.theme.colors.grey(2)};
  }
`;