import React from 'react';

export default () => <h1 style={{ padding: '1rem 2rem' }}>401 Unauthorized</h1>;
