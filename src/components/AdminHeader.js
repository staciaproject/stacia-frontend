import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box, Row } from './Box';
import { withRouter, NavLink } from 'react-router-dom';

import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';

export default class AdminHeader extends Component {
  static propTypes = {
      title: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
  };

  render() {
    const { title, icon } = this.props;
    return (
      <Wrapper>
          <HeaderCard>
            <h2><i class={icon}></i>{title}</h2>
          </HeaderCard> 
      </Wrapper>
    );
  }
}

const Wrapper = styled(Box)`
  margin: 1rem;
  padding: 0;
  flex: 1 0 21%;
  margin:0px;
`;


const HeaderCard = styled(Box)`
  width: inherit;
  height: 4rem;

  position: relative;
  background: #e1d8e2;

  h2 {
      color: white;
      margin-left:1rem;
  }
  i {
    padding-right:0.5rem;
  }
`;
