import React from 'react';
import styled from 'styled-components';
import { Box } from './Box';
import { colors } from '../styles/theme';

export const GlowingBox = styled(Box)`
  background: white;
  box-shadow: 0 1px 1px 0 ${colors.gold(0)};
  transition: 0.3s;

  :hover {
    box-shadow: 0 8px 16px 0 ${colors.gold(0)};
  }
`;
