import PropTypes from 'prop-types';
import React from 'react';
import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';
import styled from 'styled-components';
import { Box, Row } from './Box';
import MainDetails from 'src/components/MainDetails';
import { withRouter, Link } from 'react-router-dom';


export default function MainProductCard(props) {
  const { img, sale, isLiked, likes, price, rating, name, description, sku, reviews } = props;
  var id = sku;

  return (
    <Wrapper width="100%" style={{color:'#603157'}}>
      <MainProductInfo width="30%">
        <Box flexWrap="wrap">
          <h2 style={{fontWeight:'900', fontSize:'24px', marginBottom:'0.5rem'}}>{name}</h2>
          <h3 style={{color:'#603157', fontSize:'24px'}}>IDR {price}</h3>
          <div style={{borderTop:'1px solid #D3D3D3',borderBottom:'1px solid #D3D3D3', width:'inherit'}}>
            <h4 style={{color:'#603157', marginTop:'0.5rem'}}>Description:</h4>
            <div style={{marginBottom:'0.5rem'}}>{description}</div>
          </div>
        </Box>
        <ActionButtons>
          <a
            style={{cursor:'pointer', color:'#603157', textDecoration:'none'}}>
            <Link style={{textDecoration:'none'}} to={`/eyelash/${id}`}>Details</Link>
          </a>
          <Box className="action-button cart" style={{cursor:'pointer'}}>Add to Cart</Box>
        </ActionButtons>
      </MainProductInfo>
      <MainProductImage width="70%">
        <img src={img} height="400px" alt="product_image" />
      </MainProductImage>
    </Wrapper>
  );
}

MainProductCard.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  img: PropTypes.string.isRequired,
  sale: PropTypes.bool.isRequired,
  isLiked: PropTypes.bool.isRequired,
  likes: PropTypes.number.isRequired,
  rating: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  reviews: PropTypes.array.isRequired,
};

const Wrapper = styled(Row)`
  margin: 10px;
  align-items: stretch;
  width: 100%;
  justify-content: space-between;
  padding: 3rem;
  padding-right: 10rem;
`;

const MainProductInfo = styled(Box)`
  max-width: 30%:
  flex-wrap: wrap;
  justify-content: flex-end;
  padding: 30px;
`;

const MainProductImage = styled(Box)`
  overflow: hidden;
  align-items: center;
  justify-content: center;
`;

const ActionButtons = styled(Row)`
  margin-top: 2rem;
  justify-content: space-between;
  .action-button {
    width: 50%;
    align-items: center;
    padding: 0.625rem 1rem;
  }
  .cart {
    background-color: ${props => props.theme.colors.purple(0)};
    color: ${props => props.theme.colors.white};
  }
`;
