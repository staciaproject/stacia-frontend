import styled from 'styled-components';

export default styled.img`
  width: auto;
  height: auto;
  max-width: 100%;
  ${props => props.fluid && 'width: 100%'};
  ${props => props.width && `width: ${props.width}`};
  ${props => props.height && `height: ${props.height}`};
  ${props => props.radius && `border-radius: ${props.radius}`};
`;
