import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from './Box';
import { Container, Row, Col } from 'react-bootstrap';
import { withRouter, NavLink } from 'react-router-dom';
import { Table } from 'react-bootstrap';

import CreateProductQuickView from 'src/components/Modals/CreateProductQuickView';

export default class AdminCatalogCard extends Component {
  static propTypes = {
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
  };

  render() {
    const { icon, title, to } = this.props;
    return (
      <Wrapper>
        <CatalogContent>
          <Table>
              <tr>
                <td colSpan="2"><h4>{title}</h4></td>
                <td style={{width:5+'rem', color:'grey'}}><i class="fa fa-edit"></i>Edit</td>
                <td style={{width:7+'rem', color:'grey'}}><i class="fa fa-trash-alt"></i>Delete</td>
              </tr>
              <tr>
                <td><p>Total Product</p></td>
              </tr>
          </Table>
        </CatalogContent>
      </Wrapper>
    );
  }
}

const Wrapper = styled(Box)`
  margin: 1rem;
  flex: 1 0 100%;
  margin:0px;
  margin-top:1rem;
  border-top: 1px solid #e1d8e2;
`;


const CatalogContent = styled(Box)`
  width: 65rem;
  height: auto;

  position: relative;
  background: transparent;

  h4 {
    font-size: 20px;
    margin-top: 1rem;
    margin-left: 1rem;
    margin-bottom:5px;
  }

  p {
    margin-top: 0px;
    margin-left: 1rem;
    font-family: Raleway;
  }

`;
