import React from 'react';
import styled from 'styled-components';
import { withRouter, NavLink } from 'react-router-dom';
import { Box } from './Box';
import MetisMenu from 'react-metismenu';



import Admin from '../../public/img/no-image-admin.png';


const Sidebar =[
    {
        id:1,
        icon: 'fas fa-user',
        label: 'Profile',
        to: '/profile',
    },
    {
        id:2,
        icon: 'fas fa-clipboard-list',
        label: 'Daftar Transaksi',
        to: '/transactionlist',
    },
    {
        id:3,
        icon: 'fas fa-heart',
        label: 'Wishlist',
        to: '/wishlist',
    },
];

function ProfileSidebar() {
  return (
      <Bar>
        <AdminContact>
            <p>
              <img src={Admin}/>
              <h2>Stacia Sitohang</h2>
            </p>
        </AdminContact>
        <MetisMenu
          content={Sidebar} 
          activeLinkFromLocation
        />
      </Bar>
      
  );
}
const AdminContact = styled(Box)`


    img {
        border-radius: 50%;
        height: 50px;
        width: 50px;
        float: left;
        margin-left: 10%;
        margin-top: 5%;
        margin-right: 5%;
    }

    h2 {
        color: white;
        font-size: 16px;
        margin-bottom: 0px;
        margin-top: 5%;
    }
    p{
        margin-top: 5px;
        font-weight: lighter;
        color: white;
    }
`

const Bar = styled.aside`
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  width: 20%;
  background-color:#603157;
  color: #eeb124;


  .metismenu-container {
    list-style: none;
    padding-left: 0px;
    margin: 0px;
  }

  .metismenu-item {
      background-color:#562b4f;
  }


  .metismenu-link {
      color: white;
      font-size: 16px;
      text-decoration: none;
  }


  i {
      min-width: 52px;
      padding:5%;
      background-color: #44233e;
      margin-right:5%;
  }

  
`;

const SidebarItem = styled(NavLink)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  text-decoration: none;
  li {
    list-style: none;
  }

`;

const GlowingLine = styled(Box)`
  height: 5px;
  width: 100%;
  margin: 0.5rem 0;
  background-color: #eeb124;
  background-image: linear-gradient(to right, #e7d2a1, #faf8cb, #e7d2a1, #faf8cb, #e7d2a1, #faf8cb, #e7d2a1, #faf8cb);

  h1 {
    text-align: center;
    vertical-align: middle;
    line-height: 60px;   
    color: #603157;
    font-size: 30px;
    font-weight: bold;
    font-family: Raleway;
  }
`;

export default withRouter(ProfileSidebar);
