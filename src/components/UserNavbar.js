import React, { Component } from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { AvatarWrapper } from 'src/components/PageComponents';
import { Navbar, NavItem } from 'react-bootstrap';
import { MENU_TYPE } from 'src/common/constants';
import { Box, Row } from './Box';
import { privateRoutes as menu } from '../routes';


export default class UserNavbar extends Component {
    render() {
      return (
        <PageHeader {...this.props.children}>
          <BootstrapNavbar collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <img src="/img/logo.png" alt="stacia logo" height="4rem"/> 
              </Navbar.Brand>
            </Navbar.Header>
          </BootstrapNavbar>
          {menu
            .filter(item => !item.nested && item.type === MENU_TYPE.MAIN)
            .map(({ path, title }) => (
              <NavLink exact={path === '/'} to={path} key={path} className="nav-link">
                <div>{title}</div>
              </NavLink>
            ))}
          <UserButtonWrapper>
          </UserButtonWrapper>
        </PageHeader>
      );
    }
  }

const PageHeader = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${props => props.theme.colors.white};
  padding: 0;
  box-shadow: ${props => props.theme.shadows[0]};
  z-index: 1;
  max-height: 4rem;
  > * {
    max-height: 4rem;
  }
  .nav-link {
    text-decoration: None;
    color:#603157;
    font-weight: Bold;
    display: flex;
    align-items: center;
    list-style: none;
    font-family: Raleway;
    a {
      text-decoration: none;
    }
  }

  @media (max-width: 480px) {
    .nav-link {
      text-decoration: None;
      color:#603157;
      font-weight: Bold;
      display: flex;
      align-items: center;
      list-style: none;
      font-family: Raleway;
      font-size: 10px;
      margin: 8px;
      text-align: center;
      a {
        text-decoration: none;
      }
    }
    max-height: 2rem;
    > * {
      max-height: 2rem;
    }
  }
`;

const BootstrapNavbar = styled(Navbar)`
  display: flex;
  flex-direction: row;
  margin-right: 10%;
  .navbar-brand {
    height: 4rem;
  }
  .navbar-collapse {
    padding-top: 35px;
  }
  @media (max-width: 480px) {
    .navbar-brand {
      height: 2rem;
    }
  }
`;


const UserButtonWrapper = styled.div`
  display: flex;
  position: relative;
  margin-left: 10%;
  justify-content: right;

  :hover .user-dropdown {
    display: flex;
    flex: 1;
  }

  .icon {
    transition: transform 0.4s ease-in-out;
  }

  :hover .icon {
    transform: rotate(180deg);
  }
`;

const UserButton = styled(Row)`
  padding: 0 1.25rem;
  margin-left: 1.25rem;
  align-items: center;
  border-left: 1px solid #e5e5e5;

  :hover {
    background-color: #dedede;
  }

  .name {
    margin: 0.125rem 0.75rem 0;
    font-size: 0.875rem;
    color: ${props => props.theme.colors.grey(1)};
  }
`;

const UserDropdown = styled(Box)`
  display: none;
  position: absolute;
  background-color: ${props => props.theme.colors.purple(0)};
  right: 0;
  top: 100%;
`;

const DropdownItem = styled(NavLink)`
  width: 100%;
  height: 3rem;
  padding: 1rem;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  text-decoration: none;

  &:hover {
    opacity: 1;
    background-color: rgba(0, 0, 0, 0.25);
  }

  div {
    margin-left: 0.8rem;
    margin-top: 0.35rem;
    font-size: 0.875rem;
    font-weight: 600;
    color: ${props => props.theme.colors.white};
    text-decoration: none;
  }
`;
