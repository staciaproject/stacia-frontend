import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'react-images';
import isArray from 'lodash/isArray';

import Touchable from './Touchable';

import Image from 'src/components/Image';

export default class PopableImage extends Component {
  static propTypes = {
    src: PropTypes.string,
    srcSet: PropTypes.arrayOf(PropTypes.string),
    alt: PropTypes.string.isRequired,
    initialIndex: PropTypes.number,
  };

  static defaultProps = {
    initialIndex: 0,
    src: undefined,
    srcSet: undefined,
  };

  state = {
    isPopupOpen: false,
    currentImage: this.props.initialIndex,
  };

  setPopup = isPopupOpen => () => this.setState({ isPopupOpen });

  modifyIndex = modifier => () =>
    this.setState(({ currentImage }) => ({ currentImage: currentImage + modifier }));

  render() {
    const { src, srcSet, ...rest } = this.props;
    const { currentImage } = this.state;
    let images = [];
    if (isArray(srcSet)) {
      images = srcSet;
    } else {
      images = [src];
    }

    images = images.map(imageSrc => ({ src: imageSrc }));

    return (
      <React.Fragment>
        <Lightbox
          images={images}
          isOpen={this.state.isPopupOpen}
          onClose={this.setPopup(false)}
          currentImage={currentImage}
          onClickNext={this.modifyIndex(1)}
          onClickPrev={this.modifyIndex(-1)}
          showImageCount
          backdropClosesModal
          showThumbnails
        />
        <Touchable onClick={this.setPopup(true)}>
          <Image {...rest} src={images[this.props.initialIndex].src} />
        </Touchable>
      </React.Fragment>
    );
  }
}
