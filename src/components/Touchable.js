import React from 'react';
import styled from 'styled-components';
import Box from './Box';

const TouchableContainer = styled(Box)`
  cursor: pointer;
`;

const Touchable = ({ onClick, ref, ...rest }) => (
  <TouchableContainer
    onClick={e => {
      e.stopPropagation();
      onClick(e);
    }}
    innerRef={ref}
    {...rest}
  />
);

export default Touchable;
