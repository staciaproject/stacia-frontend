import kebabCase from 'lodash/kebabCase';
import styled, { css } from 'styled-components';

const mixin = prop => css`
  ${props => props[prop] && `${kebabCase(prop)}: ${props[prop]}`};
`;

const Text = styled.span`
  ${mixin('color')};
  ${mixin('fontSize')};
  ${mixin('fontWeight')};
`;

export default Text;
