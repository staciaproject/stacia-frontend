import React from 'react';
import styled from 'styled-components';
import { Box } from './Box';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';

export default function ArticleCard(props) {
  const { title,summary, sku, image, date } = props;
  return (
    <Wrapper width="100%">
      <Link style={{textDecoration:'none', color:'grey'}} to={`/tips/${sku}`}>
        <ArticleBox>
          <div className="card">
            <div className="container">
              <img src={image}>
              </img>
              <h2>
                <b>{title}</b>
              </h2>
              <p style={{textAlign:'justify'}}>
                {summary.slice(0,500)} . . .
              </p>
              <div>
                <p style={{color:'#603157', textDecoration:'none', fontSize:'12px', display: 'inline', textAlign:'left'}}>
                  Read More &#62;&#62;
                  <span style={{float:'right'}}>
                    <p style={{color:'grey', textDecoration:'none', fontSize:'12px', display: 'inline'}}> {formatDate(date)} </p>
                  </span>
                </p>
              </div>


    
              
            </div>
          </div>
        </ArticleBox>
      </Link>
    </Wrapper>
  );
}

ArticleCard.propTypes = {
  title: PropTypes.string.isRequired,
  summary: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
};

function formatDate(string){
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  return new Date(string).toLocaleDateString([],options);
}

const Wrapper = styled(Box)`
  justify-content: space-between;
`;

const ArticleBox = styled(Box)`
  cursor: pointer;
  padding-left: 5%;
  padding-right: 10%;
  width: 110%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-bottom: 50px;
  img {
      align-items: center;
      width: 100%;
      height: auto;
  }

  a {
    text-decoration: None;
    color: #000;
    font-weight: bolder;
  }

  .card {
    box-shadow: 0 4px 8px 0 rgba(229,193,0,1);
    transition: 0.3s;
  }
  
  .card:hover {
    box-shadow: 0 8px 16px 0 rgba(178,150,0,1);
  }
  
  .container {
    padding: 2px 16px;
    padding-bottom: 20px;
  }

  .alignleft {
    padding-left:0
    padding-bottom: 15px;
    border: none;
    background: none;
    float: left;
  }
  .alignright {
    float: right;
    color: 	#A9A9A9;
    padding-bottom: 10px;
  }

  p {
    line-height: 1.5;
  }

  @media (max-width: 480px) {
    h2 {
      font-size:14px;
    }
    p {
      font-size: 10px;
    }
    .alignleft {
      font-size: 10px;
    }
  }

  `;
