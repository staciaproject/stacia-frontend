import styled from 'styled-components';

export const Label = styled.label`
  width: 100%;
  display: flex;
  flex-direction: column;
  font-size: ${props => props.theme.sizes.font.reg};
  padding: ${props => props.theme.sizes.spacing.small} 0;
  > input {
    font-size: ${props => props.theme.sizes.font.reg};
    margin-top: ${props => props.theme.sizes.spacing.small};
  }
`;

export const TextInput = styled.input`
  width: 100%;
  padding: ${props => props.theme.sizes.spacing.medium};
  border: 1px solid ${props => props.theme.colors.grey(3)};
  border-radius: ${props => props.theme.sizes.radius.reg};
`;
