import React, { Component } from 'react';
import styled from 'styled-components';
import { Box, Row, Col } from './Box';

import ProductImage1 from '../../public/img/products/SR04_02.jpg';

export default class MainDetails extends Component {
  render() {
    const { sku, name, description, price, stock, images, reviews, isReviewed, likes, isLiked, rating, howToUse } = this.props;
    return (
      <Wrapper>
        <Row>
            <ProductImage>
                <img src={ProductImage1} alt="product_image"/>  
            </ProductImage>
            <ProductInfo>
                <Col>
                    <h2>{sku}</h2>
                    <h3 className="color-violet">{name}</h3>
                    <Row>
                        <div className="small">&#9733; &#9733; &#9734; &#9734; &#9734;</div>
                        <div className="small">{reviews} reviews</div>
                        <div className="small">|</div>
                        <div className="small">&#10084;</div>
                        <div className="small">{likes} loves</div>
                    </Row>
                    <p className="price">IDR {price}</p>
                </Col>
            </ProductInfo>
        </Row>
      </Wrapper>
    );
  }
}


const Wrapper = styled(Box)`
  margin: 1rem;
  width: 100%;
`;

const ProductImage = styled(Box)`
  width: 20rem;
  height: 20rem;
  overflow: hidden;
  align-items: center;
  justify-content: center;
  position: relative;
  margin-right: 1rem;
`;

const ProductInfo = styled(Box)`
    color: #603157;
    .small {
        margin-right: 0.5rem;
    }

    .color-violet {
        font-weight: normal;
        color: #603157;
    }

    .price{
        font-size: 25px;
    }
`

