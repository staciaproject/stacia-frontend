import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import styled from 'styled-components';
import Box from './Box';

class FurtherReading extends Component {
    render() {
      return (
        <Content> 
          <SeparatorLine />
          <h1 className="testimonial">F U R T H E R &ensp; R E A D I N G</h1>
          <Further responsive>
              <div className="further">
                <a href="/content"><img src="/img/articles/article4.png"></img></a>
                <div class="bottom-left"><h1>Makeup for Night Out</h1>
                <hr></hr>
                </div>
              </div>
              <div className="further">
              <a href="/content"><img src="/img/articles/article5.png"></img></a>
                <div class="bottom-left"><h1>Makeup for Night Out</h1>
                <hr></hr>
                </div>
              </div>
              <div className="further">
              <a href="/content"><img src="/img/articles/article6.png"></img></a>
                <div class="bottom-left"><h1>Makeup for Night Out</h1>
                <hr></hr>
                </div>
              </div>
              
          </Further>
        </Content>
      );
    }
};

const Content = styled(Box)`
  padding-top: 2%;
  align-items: center;
  h1 {
    font-weight: normal;
    font-family: Raleway;
    color: #603157;
  }
`;

const Further = styled(Box)`
  width: 100%;
  display: inline-block;


  .further {
    width: 33%;
    position: relative;
    display: inline-block;
    img {
      width: 100%;
    }
  }

  .bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;

    hr {
      background-color:#e7d2a1;
      height: 4px;
      width: 100%;
      border-color: transparent;
    }

    h1{
      color: white;
    }
  }

`;

const SeparatorLine = styled.hr`
  height: 2px;
  width: 5%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 50px;
  border: 0;
  background-color: #603157; 
`;

export default FurtherReading;
